import React from 'react'
import styled from 'styled-components'
import ReactLoader from 'react-loader-spinner'
import {scheme} from '../../assets/styles'

export const Loader = () => {
  return (
    <LoaderWrapper>
      <ReactLoader
        type="TailSpin"
        color={scheme.colors.white}
        height={40}
        width={40}
      />
    </LoaderWrapper>
  )
}

const LoaderWrapper = styled.div`
  width: max-content;
  margin: 0 auto;
  padding-top: 40px;
`
