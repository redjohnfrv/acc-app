import React from 'react'
import styled from 'styled-components'
import {scheme} from '../../assets/styles'
import {LOADING_STRING} from '../../constants'

type Props = {
  text?: string;
  type?: 'button' | 'reset' | 'submit'
  loading?: boolean
  disabled?: boolean
  onClick?: () => void
  icon?: string
}

export const PrimaryButton = ({
    text,
    type,
    loading = false,
    disabled = false,
    onClick,
  }: Props) => {
  return (
      <ButtonWrapper
        type={type}
        onClick={onClick}
        disabled={disabled}
      >
        {loading ? LOADING_STRING : text}
      </ButtonWrapper>
  )
}

const ButtonWrapper = styled.button<{disabled: Props}>`
  width: 100%;
  height: 46px;
  line-height: 46px;
  text-align: center;
  background: ${props => props.disabled ? scheme.colors.lightBlue : scheme.colors.blue};
  color: ${props => props.disabled ? scheme.colors.blue : scheme.colors.white};
  border: none;
  border-radius: 6px;
`
