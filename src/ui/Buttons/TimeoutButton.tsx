import React from 'react'
import {PrimaryButton} from './PrimaryButton'
import {TIME} from '../../constants'
import {useTimer} from '../../hooks'

type Props = {
  text?: string
  type?: 'button' | 'reset' | 'submit'
  disabled?: boolean
  onClick?: () => void
  timeout: number
  timeoutOnMount?: boolean
}

export const TimeoutButton = ({
    text,
    type,
    disabled,
    onClick,
    timeout,
    timeoutOnMount
  }: Props) => {

  const {timer, disableBtn} = useTimer(timeout, 'Resend Email')

  const handleClick = () => {
    sessionStorage.setItem(TIME, String(timeout))
    disableBtn.off()
    onClick && console.log(onClick())
  }

  return (
    <PrimaryButton
      text={timer}
      type={type}
      disabled={disabled || !disableBtn.isOn}
      onClick={handleClick}
    />
  )
}
