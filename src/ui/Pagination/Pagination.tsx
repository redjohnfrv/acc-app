import React from 'react'
import styled from 'styled-components'
import {images} from '../../assets/images'
import {mediaMedium, scheme} from '../../assets/styles'

type Props = {
  items: number
  currentPage: number
  itemsPerPage: number
  totalItems: number
  totalPages: number
  paginationHandler: (page: number) => void
}

export const Pagination = ({
  items,
  currentPage,
  itemsPerPage,
  totalItems,
  totalPages,
  paginationHandler,
}: Props) => {

  return (
    <Wrapper>
      <Arrow
        onClick={() => paginationHandler(currentPage - 1)}
        $revert={false}
        $disable={currentPage === 1}
      />
      <Pages>
        {itemsPerPage * (currentPage - 1) + 1}
        -
        {itemsPerPage > items
          ? items
          : currentPage * itemsPerPage
        }
        {` of `}
        {totalItems}</Pages>
      <Arrow
        onClick={() => paginationHandler(currentPage + 1)}
        $revert={true}
        $disable={Number(currentPage) === totalPages}
      />
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  
  @media (max-width: ${mediaMedium}) {
    align-self: flex-end;
  }
`
const Arrow = styled.div<{$revert: boolean, $disable: boolean}>`
  width: 10px;
  height: 18px;
  background: url(${images.icons.arrow}) center / contain no-repeat;
  transform: ${props => props.$revert ? 'rotate(180deg)' : 'rotate(0deg)'};
  opacity: ${props => props.$disable ? '.4' : '1'};
  cursor: ${props => props.$disable ? 'default' : 'pointer'};
`
const Pages = styled.span`
  padding: 0 18px;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.black};
  pointer-events: none;
`
