export const scheme = {
  colors: {
    blue: '#2BAEE0',
    lightBlue: '#CAF0FF',
    commonBlue: '#D5F3FF',
    red: '#F05658',
    lightRed: '#FFFAFA',
    black: '#122434',
    purple: '#681B51',
    gray: '#BFBFBF',
    darkGray: '#737373',
    lightGray: '#E8E8E8',
    whiteGray: '#F9F9F9',
    white: '#FFF',
  },
  size: {
    textBig: '24px',
    textNormal: '16px',
    textSmall: '12px',
    titleH1: '32px',
    titleH2: '24px',
    titleH3: '16px',
  },
}

export const mediaSmall = '375px'
export const mediaMedium = '768px'
export const mediaLarge = '1024px'
