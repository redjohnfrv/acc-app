import logo from './pics/logo.svg'
import logoDark from './pics/logo-dark.svg'
import bg from './pics/bg.svg'
import avatar from './pics/avatar.png'
import heart from './pics/heart.svg'
import heartFull from './pics/heartFull.svg'
import iconTwitter from './icons/twitter.svg'
import eye from './icons/eye.svg'
import eyeOff from './icons/eye-off.svg'
import social from './icons/social.svg'
import checkboxIn from './icons/checkboxIn.svg'
import checkboxOut from './icons/checkboxOut.svg'
import noImage from './icons/no-image.svg'
import arrow from './icons/arrow.svg'
import companyNoImage from './pics/company-no-image.png'
import dot from './icons/dot.svg'
import compLogo01 from './pics/company-logo-01.svg'
import compLogo02 from './pics/company-logo-02.svg'
import compLogo03 from './pics/company-logo-03.svg'
import compLogo04 from './pics/company-logo-04.svg'
import searching from './icons/searching.svg'
import filters from './icons/filters.svg'
import addList from './icons/addList.svg'
import support from './icons/support.svg'
import edit from './icons/edit.svg'
import menu from './icons/menu.svg'
import closeMenu from './icons/closeMenu.svg'

export const images = {
  icons: {
    iconTwitter,
    eye,
    eyeOff,
    social,
    checkboxIn,
    checkboxOut,
    noImage,
    arrow,
    dot,
    filters,
    searching,
    addList,
    support,
    edit,
    menu,
    closeMenu,
  },
  pics: {
    bg,
    logo,
    logoDark,
    avatar,
    companyNoImage,
    compLogo01,
    compLogo02,
    compLogo03,
    compLogo04,
    heart,
    heartFull,
  }
}
