import React from 'react'
import './assets/styles/index.scss'
import {Provider} from 'react-redux'
import {persistor, store} from './redux/store'
import {PersistGate} from 'redux-persist/integration/react'
import {BrowserRouter} from 'react-router-dom'
import {NavigatorMain} from './pages'
import {Toast} from './ui/Toast'

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <Toast />
          <NavigatorMain />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  )
}

export default App
