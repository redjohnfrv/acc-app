export const getProspectId = () => {

  const url = document.documentURI
  const getSliceIndex = url.lastIndexOf('/') + 1
  const id = url.slice(getSliceIndex)
  const checkId = id.indexOf('?')

  if (checkId > 0) {
    return id.slice(0, checkId)
  }

  return id
}
