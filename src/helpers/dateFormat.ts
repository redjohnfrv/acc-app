export const dateFormat = (raw: number) => {

  const fullYear = new Date(raw).getFullYear()
  const month = new Date(raw).getMonth() - 1
  const day = new Date(raw).getDate()

  enum EDays {
    January,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December,
  }

  const monthByName = EDays[month]

  return {fullYear, monthByName, day}

}
