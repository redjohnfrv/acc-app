import axios from 'axios'
import {
  METHOD_DELETE,
  METHOD_GET,
  METHOD_PATCH,
  METHOD_POST,
  URL_BASE
} from '../../constants'
import {FiltersType} from '../types'
import {getToken} from '../../helpers'

export const savedListApi = {
  getAllSavedLists: (page: number, limit: number, sort: string) => axios({
    method: METHOD_GET,
    url: `${URL_BASE}/api/v1/saved-list?page=${page}&limit=${limit}&sort=${sort}`,
    headers: {'Authorization': `Bearer ${getToken()}`},
  }),

  createSavedList: (filters: FiltersType, prospectsAvailable: number) => axios({
    method: METHOD_POST,
    url: `${URL_BASE}/api/v1/saved-list`,
    headers: {'Authorization': `Bearer ${getToken()}`},
    data: {filters, prospectsAvailable},
  }),

  updateSavedList: (id: string, filters: FiltersType, prospectsAvailable: number, name: string) => axios({
    method: METHOD_PATCH,
    url: `${URL_BASE}/api/v1/saved-list/${id}`,
    headers: {'Authorization': `Bearer ${getToken()}`},
    data: {filters, prospectsAvailable, name},
  }),

  getSavedListById: (id: string) => axios({
    method: METHOD_GET,
    url: `${URL_BASE}/api/v1/saved-list/${id}`,
    headers: {'Authorization': `Bearer ${getToken()}`},
  }),

  deleteSavedListById: (id: string) => axios({
    method: METHOD_DELETE,
    url: `${URL_BASE}/api/v1/saved-list/${id}`,
    headers: {'Authorization': `Bearer ${getToken()}`},
  }),
}

