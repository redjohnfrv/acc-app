import {createSlice, AsyncThunk, AnyAction} from '@reduxjs/toolkit'
import {
  getSavedLists,
  createSavedList,
  getSavedListById,
  deleteSavedList,
  updateSavedList
} from './thunks'
import {toast} from 'react-toastify'
import {userActions} from '../user'
import {PENDING, REJECTED, SAVED_LIST} from '../../constants'
import {SavedListStateType} from './types'

type SavedListAsyncThunk = AsyncThunk<unknown, unknown, { rejectValue: string }>
type PendingAction = ReturnType<SavedListAsyncThunk['pending']>
type RejectedAction = ReturnType<SavedListAsyncThunk['rejected']>

//FILTER FUNCTIONS
function isPendingAction(action: AnyAction): action is PendingAction {
  return action.type.startsWith(SAVED_LIST) && action.type.endsWith(PENDING)
}

function isRejectedAction(action: AnyAction): action is RejectedAction {
  return action.type.startsWith(SAVED_LIST) && action.type.endsWith(REJECTED)
}
//END OF FILTER FUNCTIONS

const initialState: SavedListStateType = {
  items: [],
  meta: null,
  loading: false,
}

const savedListSlice = createSlice({
  name: 'savedList',
  initialState,
  reducers: {},
  extraReducers: (mapBuilder) => {
    mapBuilder.addCase(getSavedLists.fulfilled, (state, action) => {
      state.items = action.payload.items
      state.meta = action.payload.meta
      state.loading = false
    })
    mapBuilder.addCase(createSavedList.fulfilled, (state, action) => {
      state.items.push(action.payload)
      toast.success('List successfully saved!')
      state.loading = false
    })
    mapBuilder.addCase(getSavedListById.fulfilled, (state, action) => {
      const isExist = !!state.items.find(savedList => savedList.id === action.payload.id)
      if (!isExist) {
        state.items.push(action.payload)
      }
      state.loading = false
    })
    mapBuilder.addCase(updateSavedList.fulfilled, (state, action) => {
      state.items = state.items.map(savedList => {
        if (savedList.id === action.payload.id) {
          return action.payload
        }
        return savedList
      })
      state.loading = false
    })
    mapBuilder.addCase(deleteSavedList.fulfilled, (state, action) => {
      state.items = state.items.filter(savedList => savedList.id !== action.payload)
      state.loading = false
    })
    mapBuilder.addCase(userActions.logout, (state) => {
      state.items = []
      state.meta = null
    })
    mapBuilder.addMatcher(isPendingAction, (state) => {
      state.loading = true
    })
    mapBuilder.addMatcher(isRejectedAction, (state, action) => {
      toast.error(action.payload)
      state.loading = false
    })
  },
})

const {actions, reducer} = savedListSlice

export const savedListActions = {
  ...actions,
  getSavedLists,
  createSavedList,
  getSavedListById,
  deleteSavedList,
  updateSavedList,
}

export default reducer
