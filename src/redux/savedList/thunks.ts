import {createAsyncThunk} from '@reduxjs/toolkit'
import {savedListApi} from './api'
import {AxiosResponse, AxiosError} from 'axios'
import {ProspectsType} from './types'
import {
  SAVED_LIST_ADD,
  SAVED_LIST_GET,
  SAVED_LIST_GET_BY_ID,
  SAVED_LIST_REMOVE,
  SAVED_LIST_UPDATE
} from '../../constants'
import {FiltersType, MetaDataType} from '../types'

export const getSavedLists = createAsyncThunk<
  {items: ProspectsType[], meta: MetaDataType},
  {page: number, limit: number, sort?: string},
  {rejectValue: string}
  >(SAVED_LIST_GET, async (data, {rejectWithValue}) => {
  try {
    const {page, limit, sort} = data
    const response: AxiosResponse = await savedListApi.getAllSavedLists(page, limit, sort!)
    return response.data
  } catch (error) {
    const {response, message} = error as AxiosError
    const errorMsg = response ? response.data.message : message
    return rejectWithValue(errorMsg)
  }
})

export const createSavedList = createAsyncThunk<
  ProspectsType,
  {filters: FiltersType, prospectsAvailable: number},
  {rejectValue: string}
  >(SAVED_LIST_ADD, async (data, {rejectWithValue}) => {
  try {
    const {filters, prospectsAvailable} = data
    const response: AxiosResponse = await savedListApi.createSavedList(filters, prospectsAvailable)
    return response.data
  } catch (error) {
    const {response, message} = error as AxiosError
    const errorMsg = response ? response.data.message : message
    return rejectWithValue(errorMsg)
  }
})

export const getSavedListById = createAsyncThunk<ProspectsType, string, {rejectValue: string}
  >(SAVED_LIST_GET_BY_ID, async (id, {rejectWithValue}) => {
    try {
      const response: AxiosResponse = await savedListApi.getSavedListById(id)
      return response.data
    } catch (error) {
      const {response, message} = error as AxiosError
      const errorMsg = response ? response.data.message : message
      return rejectWithValue(errorMsg)
    }
  }
)

export const updateSavedList = createAsyncThunk<
  ProspectsType,
  {id: string, filters: FiltersType, prospectsAvailable: number, name: string},
  {rejectValue: string}
  >(SAVED_LIST_UPDATE, async (data, {rejectWithValue}) => {
  try {
    const {id, filters, prospectsAvailable, name} = data
    const response: AxiosResponse = await savedListApi.updateSavedList(id, filters, prospectsAvailable, name)
    return response.data
  } catch(error) {
    const {response, message} = error as AxiosError
    const errorMsg = response ? response.data.message : message
    return rejectWithValue(errorMsg)
  }
})

export const deleteSavedList = createAsyncThunk<string, string, {rejectValue: string}
  >(SAVED_LIST_REMOVE, async (id, {rejectWithValue}) => {
    try {
      await savedListApi.deleteSavedListById(id)
      return id;
    } catch (error) {
      const { response, message } = error as AxiosError
      const errorMsg = response ? response.data.message : message
      return rejectWithValue(errorMsg)
    }
  }
)
