import {FiltersType, MetaDataType } from "../types"
import {UserType} from "../user/types"

export type ProspectsType = {
  id: string,
  name: string,
  filters: FiltersType,
  prospectsAvailable: number,
  lastAuthor: UserType,
  createdAt: string,
  updatedAt: string,
}

export type SavedListStateType = {
  items: ProspectsType[]
  meta: MetaDataType | null
  loading: boolean
}
