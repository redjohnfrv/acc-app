import {combineReducers, configureStore} from '@reduxjs/toolkit'
import {persistReducer, persistStore} from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import companiesReducer from './companies'
import savedListReducer from './savedList'
import teamReducer from './team'
import userReducer from './user'

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['user', 'team', 'savedList', 'companies']
}

const userConfig = {
  key: 'user',
  storage,
  blacklist: ['loading', 'error']
}

const rootReducer = combineReducers({
  user: persistReducer(userConfig, userReducer),
  savedList: savedListReducer,
  companies: companiesReducer,
  team: teamReducer,
})

// const middleware = getDefaultMiddleware({
//   serializableCheck: false,
//   immutableCheck: false,
//   thunk: true,
// })
//

export const store = configureStore({
  reducer: persistReducer(persistConfig, rootReducer),
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    serializableCheck: false,
  })
  // middleware,
})

export const persistor = persistStore(store)
