import {RootState} from '../types'

export const getUserData = (state: RootState) => state.user.user
export const isAuth = (state: RootState) => state.user.isAuth
export const isLoading = (state: RootState) => state.user.loading
