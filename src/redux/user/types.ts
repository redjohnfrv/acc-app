export type UserType = {
  id: string
  email: string
  firstName?: string
  lastName?: string
  role: string
}

export type UserStateType = {
  user: UserType | null
  isAuth: boolean
  loading: boolean
}
