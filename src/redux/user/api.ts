import axios from 'axios'
import {METHOD_POST, URL_BASE} from '../../constants'

export const userApi = {
  login: apiVariants('/api/v1/auth/sign_in'),
  signup: apiVariants('/api/v1/auth/sign_up'),
}

function apiVariants(url: string) {
  return (email: string, password: string) => axios({
    method: METHOD_POST,
    url: `${URL_BASE}${url}`,
    data: {email, password},
  })
}
