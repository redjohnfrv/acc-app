import {createAsyncThunk} from '@reduxjs/toolkit'
import {AxiosError, AxiosResponse} from 'axios'
import {userApi} from './api'
import {USER_LOGIN, USER_SIGNUP} from '../../constants'
import {AuthRequestType} from '../types'
import {UserType} from './types'
import {toast} from 'react-toastify'

export const loginUser = thunkVariants(USER_LOGIN, userApi.login)
export const signupUser = thunkVariants(USER_SIGNUP, userApi.signup)

function thunkVariants(actionType: string, cb: any) {
  return createAsyncThunk<
    {user: UserType, accessToken: string},
    AuthRequestType,
    {rejectValue: string }
    >(actionType, async ({email, password}, {rejectWithValue}) => {
    try {
      const response: AxiosResponse = await cb(email, password)
      if (actionType === USER_SIGNUP && !response.data.error)
        toast.success('Registration successful! You are logged in.')
      return response.data
    } catch (error) {
      const {response, message} = error as AxiosError
      const errorMsg = response ? response.data.message : message
      return rejectWithValue(errorMsg)
    }
  })
}
