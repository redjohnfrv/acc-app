import {AuthRequestType} from '../types'
import {AnyAction, AsyncThunk, createSlice} from '@reduxjs/toolkit'
import {toast} from 'react-toastify'
import {loginUser, signupUser} from './thunks'
import {
  FULFILLED,
  PENDING,
  REJECTED,
  USER,
  USER_LOGIN,
  USER_SIGNUP
} from '../../constants'
import {UserStateType, UserType} from './types'

type AuthAsyncThunk = AsyncThunk<
  {user: UserType, accessToken: string},
  AuthRequestType,
  {rejectValue: string}
>
type PendingAction = ReturnType<AuthAsyncThunk['pending']>
type FulfilledAction = ReturnType<AuthAsyncThunk['fulfilled']>
type RejectedAction = ReturnType<AuthAsyncThunk['rejected']>

//FILTER FUNCTIONS
function isPendingAction(action: AnyAction): action is PendingAction {
  return action.type.startsWith(USER) && action.type.endsWith(PENDING)
}

function isFulfilledAction(action: AnyAction): action is FulfilledAction {
  return action.type.startsWith(USER) && action.type.endsWith(FULFILLED)
}

function isFulfilledAuthAction(action: AnyAction): action is FulfilledAction {
  return (action.type.startsWith(USER_LOGIN)
    || action.type.startsWith(USER_SIGNUP)) && action.type.endsWith(FULFILLED)
}

function isRejectedAction(action: AnyAction): action is RejectedAction {
  return action.type.startsWith(USER) && action.type.endsWith(REJECTED)
}
//END OF FILTER FUNCTIONS

const initialState: UserStateType = {
  user: null,
  isAuth: !!localStorage.getItem('token'),
  loading: false,
}

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    logout: (state) => {
      state.user = null
      state.isAuth = false
      localStorage.removeItem('token')
    },
  },
  extraReducers: (mapBuilder) => {
    mapBuilder.addMatcher(isPendingAction, (state) => {
      state.loading = true
    })
    mapBuilder.addMatcher(isFulfilledAuthAction, (state, action) => {
      const {user, accessToken} = action.payload
      state.user = user
      state.isAuth = true
      localStorage.setItem('token', accessToken)
    })
    mapBuilder.addMatcher(isFulfilledAction, (state) => {
      state.loading = false
    })
    mapBuilder.addMatcher(isRejectedAction, (state, action) => {
      toast.error(action.payload)
      state.isAuth = false
      state.loading = false
      localStorage.removeItem('token')
    })
  },
})

const {actions, reducer} = userSlice

export const userActions = {...actions, loginUser, signupUser}

export default reducer
