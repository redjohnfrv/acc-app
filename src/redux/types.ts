import {store} from './store'

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export type AuthRequestType = {
  email: string
  password: string
}

export type MetaDataType = {
  totalItems: number
  itemCount: number
  itemsPerPage: string
  totalPages: number
  currentPage: string
}

export type FiltersType = {
  q?: string
  deleteIds?: string[]
  industry?: string[]
  location?: string[]
  scope?: string
  totalAnnualContributors?: string
  revenueMin?: string
  revenueMax?: string
  csrFocusIds?: number[]
  affinities?: string[]
  gender?: 'male' | 'female' | 'both'
  ethnicities?: string[]
  ageRanges?: string[]
  income?: string[]
  sdgGoals?: string[]
}

