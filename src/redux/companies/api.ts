import axios from 'axios'
import {METHOD_GET, URL_BASE} from '../../constants'
import queryString from 'query-string'
import {getToken} from '../../helpers'
import {FiltersType} from '../types'

export const companiesApi = {
  getFavouriteCompanies: (page: number, limit: number) => axios({
    method: METHOD_GET,
    url: `${URL_BASE}/api/v1/companies/favorites?page=${page}&limit=${limit}`,
    headers: {'Authorization': `Bearer ${getToken()}`},
  }),

  getCompanies: (page: number, limit: number, filters?: FiltersType) => axios({
    method: METHOD_GET,
    url: `${URL_BASE}/api/v1/companies?${queryString.stringify({page, limit, ...filters})}`,
    headers: {'Authorization': `Bearer ${getToken()}`},
  }),

  getCompanyById: (id: string) => axios({
    method: METHOD_GET,
    url: `${URL_BASE}/api/v1/companies/${id}`,
    headers: {'Authorization': `Bearer ${getToken()}`},
  }),

  likeCompany: (id: string) => axios({
    method: METHOD_GET,
    url: `${URL_BASE}/api/v1/companies/${id}/like`,
    headers: {'Authorization': `Bearer ${getToken()}`},
  }),

  dislikeCompany: (id: string) => axios({
    method: METHOD_GET,
    url: `${URL_BASE}/api/v1/companies/${id}/dislike`,
    headers: {'Authorization': `Bearer ${getToken()}`},
  }),
}
