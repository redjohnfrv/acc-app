import {createAsyncThunk} from '@reduxjs/toolkit'
import {AxiosResponse, AxiosError} from 'axios'
import {companiesApi} from './api'
import {CompanyType} from './types'
import {FiltersType, MetaDataType} from '../types'
import {
  COMPANIES_DISLIKE,
  COMPANIES_GET,
  COMPANIES_GET_BY_ID,
  COMPANIES_GET_FAVOURITES,
  COMPANIES_LIKE
} from '../../constants'

export const getFavouriteCompanies = createAsyncThunk<
  {items: CompanyType[], meta: MetaDataType},
  {page: number, limit: number},
  {rejectValue: string}
  >(COMPANIES_GET_FAVOURITES, async (data, {rejectWithValue}) => {
    try {
      const {page, limit} = data
      const response: AxiosResponse = await companiesApi.getFavouriteCompanies(page, limit)
      return response.data
    } catch (error) {
      const {response, message} = error as AxiosError
      const errorMsg = response ? response.data.message : message
      return rejectWithValue(errorMsg)
    }
  }
);

export const getCompanies = createAsyncThunk<
  {items: CompanyType[], meta: MetaDataType},
  {page: number, limit: number, filters?: FiltersType},
  {rejectValue: string}
  >(COMPANIES_GET, async (data, {rejectWithValue}) => {
    try {
      const {page, limit, filters} = data
      const response: AxiosResponse = await companiesApi.getCompanies(page, limit, filters)
      return response.data
    } catch (error) {
      const { response, message } = error as AxiosError
      const errorMsg = response ? response.data.message : message
      return rejectWithValue(errorMsg)
    }
  }
)

export const getCompanyById = createAsyncThunk<CompanyType, string, {rejectValue: string}
  >(COMPANIES_GET_BY_ID, async (id, {rejectWithValue}) => {
    try {
      const response: AxiosResponse = await companiesApi.getCompanyById(id)
      return response.data
    } catch (error) {
      const {response, message} = error as AxiosError
      const errorMsg = response ? response.data.message : message
      return rejectWithValue(errorMsg)
    }
  }
)

export const likeCompany = createAsyncThunk<void, string, {rejectValue: string}
  >(COMPANIES_LIKE, async (companyId, {rejectWithValue}) => {
    try {
      await companiesApi.likeCompany(companyId)
    } catch (error) {
      const {response, message} = error as AxiosError
      const errorMsg = response ? response.data.message : message
      return rejectWithValue(errorMsg)
    }
  }
)

export const dislikeCompany = createAsyncThunk<void, string, {rejectValue: string}
  >(COMPANIES_DISLIKE, async (companyId: string, {rejectWithValue}) => {
    try {
      await companiesApi.dislikeCompany(companyId)
    } catch (error) {
      const {response, message} = error as AxiosError
      const errorMsg = response ? response.data.message : message
      return rejectWithValue(errorMsg)
    }
  }
)
