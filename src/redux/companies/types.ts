import {MetaDataType} from '../types'

export type CompanyType = {
  id: string
  name: string
  logo?: string
  score: number
  crsFocus: string[]
  like: boolean
  ticker?: string
  phone?: string
  website?: string
  city?: string
  street?: string
  state?: string
  zipCode?: number
  country?: string
  descriptionList?: string
  revenueRange?: string
  employeeRange?: number
  primaryIndustry?: string[]
  revenue?: number
  employeeCount?: number
  annualContributions?: number
  cashContributions?: number
  inKindContributions?: number
  employeeContributions?: number
  sdgGoals?: []
  charitablePartners?: string[]
  similarCompanies?: string[]
  typesOfInvestment?: string[]
}

export type CompaniesStateType = {
  companies: {
    items: CompanyType[]
    meta: MetaDataType | null
  },
  favourites: {
    items: CompanyType[]
    meta: MetaDataType | null
  }
  loading: boolean
}
