import {createSlice, AsyncThunk, AnyAction} from '@reduxjs/toolkit'
import {
  getFavouriteCompanies,
  getCompanies,
  likeCompany,
  dislikeCompany,
  getCompanyById,
} from './thunks'
import {toast} from 'react-toastify'
import {
  COMPANIES,
  COMPANIES_ASYNC,
  PENDING,
  REJECTED
} from '../../constants'
import {CompaniesStateType} from './types'
import {userActions} from '../user'

type SavedListAsyncThunk = AsyncThunk<unknown, unknown, {rejectValue: string}>
type PendingAction = ReturnType<SavedListAsyncThunk['pending']>
type RejectedAction = ReturnType<SavedListAsyncThunk['rejected']>

function isPendingAction(action: AnyAction): action is PendingAction {
  return action.type.startsWith(COMPANIES_ASYNC) && action.type.endsWith(PENDING)
}

function isRejectedAction(action: AnyAction): action is RejectedAction {
  return action.type.startsWith(COMPANIES) && action.type.endsWith(REJECTED)
}

const initialState: CompaniesStateType = {
  companies: {
    items: [],
    meta: null,
  },
  favourites: {
    items: [],
    meta: null,
  },
  loading: false,
};

const companiesSlice = createSlice({
  name: 'companies',
  initialState,
  reducers: {},
  extraReducers: (mapBuilder) => {
    mapBuilder.addCase(getCompanies.fulfilled, (state, action) => {
      const {items, meta} = action.payload
      state.companies.items = items
      state.companies.meta = meta
      state.loading = false
    })
    mapBuilder.addCase(getFavouriteCompanies.fulfilled, (state, action) => {
      const {items, meta} = action.payload
      state.favourites.items = items
      state.favourites.meta = meta
      state.loading = false
    })
    mapBuilder.addCase(getCompanyById.fulfilled, (state, action) => {
      const isExist = !!state.companies.items.find(company => company.id === action.payload.id)
      if (!isExist) {
        state.companies.items.push(action.payload)
      }
      state.loading = false
    })
    mapBuilder.addCase(likeCompany.pending, (state, { meta }) => {
      state.companies.items = state.companies.items.map(company => {
        if (company.id === meta.arg) {
          company.like = true
        }
        return company
      })
    })
    mapBuilder.addCase(dislikeCompany.pending, (state, { meta }) => {
      state.companies.items = state.companies.items.map(company => {
        if (company.id === meta.arg) {
          company.like = false
        }
        return company
      })
      state.favourites.items = state.favourites.items.filter(company => company.id !== meta.arg)
    })
    mapBuilder.addCase(userActions.logout, (state) => {
      state.favourites.items = []
      state.favourites.meta = null
    })
    mapBuilder.addMatcher(isPendingAction, (state) => {
      state.loading = true
    })
    mapBuilder.addMatcher(isRejectedAction, (state, action) => {
      toast.error(action.payload)
      state.loading = false
    })
  },
})

const {actions, reducer} = companiesSlice

export const companiesActions = {
  ...actions,
  getFavouriteCompanies,
  getCompanies,
  likeCompany,
  dislikeCompany,
  getCompanyById,
}

export default reducer
