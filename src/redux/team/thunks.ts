import {createAsyncThunk} from '@reduxjs/toolkit'
import {AxiosResponse, AxiosError} from 'axios'
import {teamApi} from './api'
import {LastLoginType, TeamType} from './types'
import {TEAM_GET, TEAM_LAST_LOGIN_GET, TEAM_REPORTS_GET} from '../../constants'

export const getTeam = createAsyncThunk<TeamType, void, {rejectValue: string}
  >(TEAM_GET, async (_, {rejectWithValue}) => {
    try {
      const response: AxiosResponse = await teamApi.getTeam()
      return response.data as TeamType
    } catch (error) {
      const {response, message} = error as AxiosError
      const errorMsg = response ? response.data.message : message
      return rejectWithValue(errorMsg)
    }
  }
)

export const getLastLogins = createAsyncThunk<LastLoginType[], void, {rejectValue: string}
  >(TEAM_LAST_LOGIN_GET, async (_, {rejectWithValue}) => {
    try {
      const response: AxiosResponse = await teamApi.getLastLogins()
      return response.data as LastLoginType[]
    } catch (error) {
      const {response, message} = error as AxiosError
      const errorMsg = response ? response.data.message : message
      return rejectWithValue(errorMsg)
    }
  }
)

export const getReports = createAsyncThunk<
  {team: TeamType, lastLogins: LastLoginType[]}, void, {rejectValue: string}
  >(TEAM_REPORTS_GET, async (_, {rejectWithValue}) => {
  try {
    const [teamResponse, lastLoginsResponse]: AxiosResponse[] = await Promise.all([
      teamApi.getTeam(),
      teamApi.getLastLogins(),
    ])
    return {
      team: teamResponse.data,
      lastLogins: lastLoginsResponse.data,
    }
  } catch (error) {
    const {response, message} = error as AxiosError;
    const errorMsg = response ? response.data.message : message
    return rejectWithValue(errorMsg)
  }
})
