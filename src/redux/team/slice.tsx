import {createSlice, AsyncThunk, AnyAction} from '@reduxjs/toolkit'
import {getTeam, getLastLogins, getReports} from './thunks'
import {toast} from 'react-toastify'
import {PENDING, REJECTED, TEAM} from '../../constants'
import {LastLoginType, TeamStateType, TeamType} from './types'
import {userActions} from '../user'

type TeamAsyncThunk = AsyncThunk<TeamType | LastLoginType[], void, {rejectValue: string}>
type PendingAction = ReturnType<TeamAsyncThunk['pending']>
type RejectedAction = ReturnType<TeamAsyncThunk['rejected']>

function isPendingAction(action: AnyAction): action is PendingAction {
  return action.type.startsWith(TEAM) && action.type.endsWith(PENDING)
}

function isRejectedAction(action: AnyAction): action is RejectedAction {
  return action.type.startsWith(TEAM) && action.type.endsWith(REJECTED)
}

const initialState: TeamStateType = {
  team: null,
  lastLogins: [],
  loading: false,
}

const teamSlice = createSlice({
  name: 'team',
  initialState,
  reducers: {},
  extraReducers: (mapBuilder) => {
    mapBuilder.addCase(getTeam.fulfilled, (state, action) => {
      state.team = action.payload
      state.loading = false
    })
    mapBuilder.addCase(getLastLogins.fulfilled, (state, action) => {
      state.lastLogins = action.payload
      state.loading = false
    })
    mapBuilder.addCase(getReports.fulfilled, (state, action) => {
      state.team = action.payload.team
      state.lastLogins = action.payload.lastLogins
      state.loading = false
    })
    mapBuilder.addCase(userActions.logout, (state) => {
      state.team = null
      state.lastLogins = []
    })
    mapBuilder.addMatcher(isPendingAction, (state) => {
      state.loading = true
    })
    mapBuilder.addMatcher(isRejectedAction, (state, action) => {
      toast.error(action.payload)
      state.loading = false
    })
  },
})

const {actions, reducer} = teamSlice

export const teamActions = {...actions, getTeam, getLastLogins, getReports}

export default reducer
