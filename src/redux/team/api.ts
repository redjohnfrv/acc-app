import axios from 'axios'
import {METHOD_GET, URL_BASE} from '../../constants'
import {getToken} from '../../helpers'

export const teamApi = {
  getTeam: apiVariant('/api/v1/team'),
  getLastLogins: apiVariant('/api/v1/team/last_logins'),
}

function apiVariant(url: string) {
  return () => axios({
    method: METHOD_GET,
    url: `${URL_BASE}${url}`,
    headers: {'Authorization': `Bearer ${getToken()}`},
  })
}
