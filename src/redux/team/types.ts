import {UserType} from '../user/types'

export type TeamType = {
  id: string
  searchCount: number
  pitchCount: number
}

export type LastLoginType = {
  user: UserType
  id: string
  loggedInAt: string
}

export type TeamStateType = {
  team: TeamType | null
  lastLogins: LastLoginType[]
  loading: boolean
}
