//COMMON STRINGS
export const EMPTY_STRING = ''
export const LOADING_STRING = 'Loading...'
export const NO_NAME = 'No Name'

//BUTTONS TYPE
export const SUBMIT = 'submit'

//ROUTES
export const BLANK = '#'
export const TO_ROOT = '/'
export const TO_LOGIN = '/login'
export const TO_REGISTER = '/signup'
export const TO_RESET = '/reset'
export const TO_RESEND_LINK = '/resend_link'
export const TO_NEW_PASSWORD = '/change_password'
export const TO_DASHBOARD = '/dashboard'
export const TO_PROSPECTS = '/prospects'
export const TO_SEARCH = '/search'

//API
export const URL_BASE = 'https://accelerist.herokuapp.com'

//METHODS
export const METHOD_GET = 'GET'
export const METHOD_POST = 'POST'
export const METHOD_PATCH = 'PATCH'
export const METHOD_DELETE = 'DELETE'

//REDUX TYPES
export const USER = 'user/'
export const USER_LOGIN = 'user/login'
export const USER_SIGNUP = 'user/signup'
export const SAVED_LIST = 'savedList/'
export const SAVED_LIST_GET = 'savedList/savedListsGet'
export const SAVED_LIST_ADD = 'savedList/savedListAdd'
export const SAVED_LIST_GET_BY_ID = 'savedList/savedListGetById'
export const SAVED_LIST_UPDATE = 'savedList/savedListUpdate'
export const SAVED_LIST_REMOVE = 'savedList/savedListRemove'
export const COMPANIES = 'companies/'
export const COMPANIES_ASYNC = 'companies/async/'
export const COMPANIES_GET_FAVOURITES = 'companies/async/favouritesGet'
export const COMPANIES_GET = 'companies/async/companiesGet'
export const COMPANIES_GET_BY_ID = 'companies/async/companyGetById'
export const COMPANIES_LIKE = 'companies/companyLike'
export const COMPANIES_DISLIKE = 'companies/companyDislike'
export const TEAM = 'team/'
export const TEAM_GET = 'team/teamGet'
export const TEAM_LAST_LOGIN_GET = 'team/lastLoginsGet'
export const TEAM_REPORTS_GET = 'team/reportsGet'

export const PENDING = '/pending'
export const FULFILLED = '/fulfilled'
export const REJECTED = '/rejected'

//LOCAL & SESSION STORAGES
export const TIME = 'time'

//SORTING TYPES
export const sort = {
  ALPHABET: 'alphabet',
  AVAILABLE: 'available',
  LAST_ACTIVITY: 'last-activity',
}
export enum ESort {
  Alphabet = 'alphabet',
  Available = 'available',
  LastActivity = 'last-activity',
}
