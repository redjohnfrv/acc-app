import React, {ChangeEvent, useEffect, useState} from 'react'
import styled from 'styled-components'
import {Header} from '../components/Header'
import {Container, FilterBox, Title} from '../ui'
import {Pagination} from '../../../ui/Pagination'
import {Loader} from '../../../ui/Loader'
import {CompaniesList} from '../components/CompaniesList'
import {mediaMedium, scheme} from '../../../assets/styles'
import {images} from '../../../assets/images'
import {NO_NAME} from '../../../constants'
import {getProspectId, isObjEmpty} from '../../../helpers'
import {useLocation, useNavigate} from 'react-router-dom'
import queryString from 'query-string'
import {useAppDispatch, useAppSelector} from '../../../redux/hooks'
import {companiesActions, companiesSelectors} from '../../../redux/companies'
import {savedListActions, savedListSelectors} from '../../../redux/savedList'
import {useSwitcher} from '../../../hooks'

export const Prospect = () => {
  const dispatch = useAppDispatch()
  const prospectId = getProspectId()
  const navigate = useNavigate()
  const location = useLocation()
  const {pathname, search} = useLocation()
  const {page} = queryString.parse(location.search)

  const prospect = useAppSelector(state => savedListSelectors.getSavedListById(state, prospectId))
  const companies = useAppSelector(companiesSelectors.getCompanies)
  const companiesMeta = useAppSelector(companiesSelectors.getCompaniesMeta)
  const isLoading = useAppSelector(companiesSelectors.isLoading)

  const isEditMode = useSwitcher()
  const [inputValue, setInputValue] = useState(prospect?.name)

  const filteredFilters = prospect && prospect.filters && !isObjEmpty(prospect.filters)
    ? Object.entries(prospect.filters).filter(item => item[0] !== 'deleteIds').slice(0, 5)
    : []

  const paginationHandler = (page: number) => {
    if (page > 0 && page <= companiesMeta.totalPages) {
      const params = {...queryString.parse(search), page}
      dispatch(companiesActions.getCompanies({page: page, limit: 12, filters: prospect.filters}))
      navigate(`${pathname}?${queryString.stringify(params)}`)
    }
  }

  useEffect(() => {
    if (!prospect) {
      dispatch(savedListActions.getSavedListById(prospectId))
    }
  }, [])

  useEffect(() => {
    if (prospect) {
      dispatch(companiesActions.getCompanies({page: Number(page) || 1, limit: 12, filters: prospect.filters}))
    }
  }, [prospect, page])

  const onChangeInput = (e: ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.target.value || NO_NAME)
  }

  const editSaveHandler = () => {
    const payload = {
      ...prospect,
      name: inputValue,
    }
    dispatch(savedListActions.updateSavedList(payload))
    isEditMode.off()
  }

  const deleteProspectHandler = () => {
    dispatch(savedListActions.deleteSavedList(prospect?.id)).then(action => {
      if (action.type.endsWith('/fulfilled')) {
       navigate('/prospects')
      }
    })
  }

  return (
    <>
      <Header />
      <Container>
        <Title title={inputValue || prospect?.name} isInput={isEditMode.isOn} onChange={onChangeInput}>
          <ProspectEdit>
            {isEditMode.isOn
              ? <SaveButton onClick={editSaveHandler}>Save</SaveButton>
              : <>
                  <EditBtn onClick={() => isEditMode.on()}>Edit</EditBtn>
                  <DeleteBtn onClick={deleteProspectHandler}>Delete</DeleteBtn>
                </>
            }
          </ProspectEdit>
        </Title>
        <Wrapper>
          <CompaniesValue>{companiesMeta?.totalItems} companies</CompaniesValue>
          <AppliedFilters>
            <AppliedFiltersTitle>Filters</AppliedFiltersTitle>
            <AppliedFiltersList>
              {filteredFilters
                && filteredFilters.map((item, index) =>
                  <FilterBox key={index} theme="white">{item[0]} : {item[1]}</FilterBox>
              )}
            </AppliedFiltersList>
          </AppliedFilters>
          <NavMenu>
            {companiesMeta && <Pagination
              items={companies.length}
              currentPage={Number(companiesMeta.currentPage)}
              itemsPerPage={Number(companiesMeta.itemsPerPage)}
              totalItems={companiesMeta.totalItems}
              totalPages={companiesMeta.totalPages}
              paginationHandler={paginationHandler}
            />}
          </NavMenu>
          {isLoading
            ? <Loader />
            : <CompaniesList companies={companies} />
          }
        </Wrapper>
      </Container>
    </>
  )
}

const Wrapper = styled.div`
  padding: 32px 0;
  display: flex;
  flex-direction: column;
`
const ProspectEdit = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  gap: 8px;
  
  @media (max-width: ${mediaMedium}) {
    margin-top: 12px;
    justify-content: flex-start;
  }
`
const EditBtn = styled.button`
  position: relative;
  height: 36px;
  width: max-content;
  padding: 0 16px 0 36px;
  line-height: 36px;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.black};
  background: ${scheme.colors.white};
  border: 1px solid ${scheme.colors.lightBlue};
  border-radius: 6px;
  cursor: pointer;
  transition: all .2s ease-in;
  
  &:before {
    content: '';
    position: absolute;
    top: 9px;
    left: 12px;
    width: 16px;
    height: 16px;
    background: url(${images.icons.edit}) center / cover no-repeat;
  }
  
  &:hover {
    background: ${scheme.colors.lightBlue};
    border: 1px solid ${scheme.colors.blue};
  }
`
const DeleteBtn = styled(EditBtn)`
  padding: 0 20px;
  color: ${scheme.colors.red};
  background: ${scheme.colors.lightRed};
  border: 1px solid ${scheme.colors.white};
  
  &:before {
    content: none;
  }

  &:hover {
    color: ${scheme.colors.black};
    background: ${scheme.colors.red};
    border: 1px solid ${scheme.colors.red};
  }
`
const SaveButton = styled(EditBtn)`
  padding: 0 20px;
  
  &:before {
    content: none;
  }
`
const CompaniesValue = styled.div`
  width: 100%;
  margin-bottom: 26px;
  font-family: 'Rubik-Medium', sans-serif;
  font-size: ${scheme.size.textNormal};
  color: ${scheme.colors.black};
`
const AppliedFilters = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 27px;
`
const AppliedFiltersTitle = styled.span`
  margin-bottom: 8px;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.darkGray};
`
const AppliedFiltersList = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 8px;
  padding-left: 8px;
`
const NavMenu = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 100%;
  margin-bottom: 27px;
`
