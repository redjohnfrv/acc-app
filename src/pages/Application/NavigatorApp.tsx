import React from 'react'
import {Navigate, Route, Routes} from 'react-router-dom'
import {Dashboard} from './Dashboard'
import {Prospects} from './Prospects'
import {Prospect} from './Prospect'
import {Search} from './Search'
import {
  TO_DASHBOARD,
  TO_LOGIN,
  TO_PROSPECTS,
  TO_REGISTER,
  TO_ROOT,
  TO_SEARCH
} from '../../constants'

export const NavigatorApp = () => {

  return (
    <Routes>
      <Route path={TO_DASHBOARD} element={<Dashboard />} />
      <Route path={TO_PROSPECTS} element={<Prospects />} />
      <Route path={`${TO_PROSPECTS}/:prospectId`} element={<Prospect />} />
      <Route path={TO_SEARCH} element={<Search />} />
      <Route path={TO_LOGIN} element={<Navigate replace to={TO_DASHBOARD} />} />
      <Route path={TO_REGISTER} element={<Navigate replace to={TO_DASHBOARD} />} />
      <Route path={TO_ROOT} element={<Navigate replace to={TO_DASHBOARD} />} />
    </Routes>
  )
}
