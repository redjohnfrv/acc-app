import React from 'react'
import styled from 'styled-components'
import {Header} from '../components/Header'
import {Container, Title} from '../ui'
import {SearchForm} from './components/SearchForm'
import {CompaniesList} from '../components/CompaniesList'
import {Loader} from '../../../ui/Loader'
import {Pagination} from '../../../ui/Pagination'
import {mediaMedium, scheme} from '../../../assets/styles'
import {images} from '../../../assets/images'
import {useAppDispatch, useAppSelector} from '../../../redux/hooks'
import {companiesActions, companiesSelectors} from '../../../redux/companies'
import {savedListActions} from '../../../redux/savedList'
import {useLocation, useNavigate} from 'react-router-dom'
import queryString from 'query-string'

export const Search = () => {

  const navigate = useNavigate()
  const {pathname, search} = useLocation()
  const {page, ...filters} = queryString.parse(search)

  const dispatch = useAppDispatch()
  const companies = useAppSelector(companiesSelectors.getCompanies)
  const companiesMeta = useAppSelector(companiesSelectors.getCompaniesMeta)
  const isLoading = useAppSelector(companiesSelectors.isLoading)

  const paginationHandler = (page: number) => {
    if (page > 0 && page <= companiesMeta.totalPages) {
      const params = {...queryString.parse(search), page}
      dispatch(companiesActions.getCompanies({page: page, limit: 12, filters}))
      navigate(`${pathname}?${queryString.stringify(params)}`)
    }
  }

  const handleSaveList = () => {
    dispatch(savedListActions.createSavedList({filters, prospectsAvailable: companiesMeta.totalItems}))
  }

  return (
    <>
      <Header />
      <Container>
        <Title title="Search">
          <SearchForm />
        </Title>
        <Wrapper>
          <Found>
            Found {companiesMeta?.totalItems || 0} companies
          </Found>
          <NavMenu>
            <NavButtons>
              <NavButtonsItem
                onClick={handleSaveList}
              >
                <img src={images.icons.addList} alt="save current list" />
                <span>Save List</span>
              </NavButtonsItem>
              <NavButtonsItem>
                <img src={images.icons.support} alt="contacting support" />
                <span>Support</span>
              </NavButtonsItem>
            </NavButtons>
            {companiesMeta &&
              <Pagination
                items={companies.length}
                currentPage={Number(companiesMeta.currentPage)}
                itemsPerPage={Number(companiesMeta.itemsPerPage)}
                totalItems={companiesMeta.totalItems}
                totalPages={companiesMeta.totalPages}
                paginationHandler={paginationHandler}
              />
            }
          </NavMenu>
          {isLoading
            ? <Loader/>
            : <CompaniesList companies={companies} />
          }
        </Wrapper>
      </Container>
    </>
  )
}

const Wrapper = styled.div`
  padding: 32px 0;
  display: flex;
  flex-direction: column;
`
const Found = styled.div`
  width: 100%;
  margin-bottom: 26px;
  font-family: 'Rubik-Medium', sans-serif;
  font-size: ${scheme.size.textNormal};
  color: ${scheme.colors.black};
`
const NavMenu = styled.div`
  display: flex;
  justify-content: space-between;
  
  @media (max-width: ${mediaMedium}) {
    margin-bottom: 12px;
  }
`
const NavButtons = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  gap: 37px;
`
const NavButtonsItem = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  gap: 9px;
  margin-bottom: 27px;
  cursor: pointer;
  
  & img {
    width: 24px;
    height: 24px;
  }
  
  & span {
    font-size: ${scheme.size.textSmall};
    color: ${scheme.colors.black};
  }
`

