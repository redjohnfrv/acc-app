import React from 'react'
import styled from 'styled-components'
import {Field, Form} from 'react-final-form'
import {images} from '../../../../../assets/images'
import {TO_SEARCH} from '../../../../../constants'
import {useNavigate} from 'react-router-dom'
import {useAppDispatch} from '../../../../../redux/hooks'
import {companiesActions} from '../../../../../redux/companies'
import {FiltersType} from '../../../../../redux/types'
import queryString from 'query-string'
import {mediaMedium, scheme} from '../../../../../assets/styles'

export const SearchForm = () => {

  const navigate = useNavigate()
  const dispatch = useAppDispatch()

  const onSubmit = (filters: FiltersType) => {
    navigate(`${TO_SEARCH}?${queryString.stringify(filters)}`)
    dispatch(companiesActions.getCompanies({page: 1, limit: 12, filters}))
  }

  return (
    <Form
      onSubmit={onSubmit}
      render={({handleSubmit, form, submitting, pristine }) => (
        <FormWrapper onSubmit={handleSubmit}>
          <Field
            name="q"
            render={({input}) => {
              return (
                <>
                  <SearchInput {...input} placeholder="Search" />
                  <SearchInputButtons>
                    <SearchInputButtonsItem
                      image="filters"
                      type="button"
                      onClick={() => console.log('show filters')}
                    />
                    <SearchInputButtonsItem
                      image="searching"
                      type="submit"
                      disabled={submitting || pristine}
                    />
                  </SearchInputButtons>
                </>
              )
            }}
          />
        </FormWrapper>
      )}
    />
  )
}

const FormWrapper = styled.form`
  position: relative;
  width: 100%;
  max-width: 715px;
`
const SearchInput = styled.input`
  width: 100%;
  max-width: 715px;
  height: 36px;
  padding: 0 24px;
  line-height: 36px;
  background: #F1F4F5;
  border: none;
  border-radius: 6px;
  
  @media (max-width: ${mediaMedium}) {
    font-size: ${scheme.size.textNormal};
  }
`
const SearchInputButtons = styled.div`
  display: flex;
  gap: 18px;
  align-items: center;
  position: absolute;
  left: calc(100% - 65px);
  top: 11px;
`
const SearchInputButtonsItem = styled.button<{image: 'filters' | 'searching'}>`
  width: 16px;
  height: 16px;
  background: ${props => props.image === 'filters' 
          ? `url(${images.icons.filters}) center / cover no-repeat;`
          : `url(${images.icons.searching}) center / cover no-repeat;`
  }
  border: none;
  cursor: pointer;
`
