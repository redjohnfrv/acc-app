import React, {ReactNode} from 'react'
import styled from 'styled-components'
import {mediaMedium, scheme} from '../../../assets/styles'

type Props = {
  children: ReactNode
}

export const Container = ({children}: Props) => {
  return (
    <Wrapper>
      {children}
    </Wrapper>
  )
}

const Wrapper = styled.div`
  width: 100%;
  min-height: 100vh;
  padding: 0 60px 60px 60px;
  background-color: ${scheme.colors.lightGray};
  
  @media (max-width: ${mediaMedium}) {
    padding: 0 20px 20px 20px;
  }
`
