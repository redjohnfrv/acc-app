import React, {ReactNode} from 'react'
import styled from 'styled-components'
import {scheme} from '../../../assets/styles'

type Props = {
  children: ReactNode
  theme?: 'standard' | 'white'
}

export const FilterBox = ({children, theme = 'standard'}: Props) => {
  return <Wrapper theme={theme}>{children}</Wrapper>
}

const Wrapper = styled.span<{theme: 'standard' | 'white'}>`
  height: 30px;
  width: max-content;
  margin-right: 6px;
  padding: 0 10px;
  line-height: 30px;
  text-align: center;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.black};
  background: ${props => props.theme === 'white' ? `${scheme.colors.white}` : 'none'};
  border: 1px solid ${scheme.colors.lightBlue};
  border-radius: 6px;
`
