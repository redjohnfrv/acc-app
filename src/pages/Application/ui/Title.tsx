import React, {ChangeEvent} from 'react'
import styled from 'styled-components'
import {mediaMedium, scheme} from '../../../assets/styles'
import {NO_NAME} from '../../../constants'

type Props = {
  title: string
  isInput?: boolean
  children?: React.ReactNode
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void
}

export const Title = ({title, children, isInput, onChange}: Props) => {

  return (
    <Wrapper>
      {isInput
        ? <TitleInput
            type="text"
            placeholder={NO_NAME}
            value={title}
            onChange={onChange}
            autoFocus
          />
        : <TitleText>{title}</TitleText>
      }
      {children}
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  width: calc(100% + 120px);
  min-height: 96px;
  margin-left: -60px;
  padding: 24px 60px;
  background-color: ${scheme.colors.white};
  
  @media (max-width: ${mediaMedium}) {
    flex-direction: column;
    align-items: flex-start;
    width: calc(100% + 40px);
    margin-left: -20px;
    padding: 24px;
  }
`
const TitleText = styled.h1`
  margin-right: 85px;
  font-size: ${scheme.size.titleH1};
  line-height: 150%;
  color: ${scheme.colors.black};
  white-space: nowrap;

  @media (max-width: ${mediaMedium}) {
    margin-right: 0;
  }
`
const TitleInput = styled.input`
  margin-right: 85px;
  padding-left: 4px;
  font-size: ${scheme.size.titleH1};
  line-height: 150%;
  color: ${scheme.colors.black};
  white-space: nowrap;
  border: none;
  background: ${scheme.colors.whiteGray};
  
  @media (max-width: ${mediaMedium}) {
    width: 100%;
  }
`
