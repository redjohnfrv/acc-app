import React, {ReactNode} from 'react'
import styled from 'styled-components'
import {scheme} from '../../../assets/styles'

type Props = {
  children: ReactNode
}

export const ContentBlock = ({children}: Props) => {
  return (
    <ContentBlockWrapper>
      {children}
    </ContentBlockWrapper>
  )
}

const ContentBlockWrapper = styled.div`
  width: 100%;
  padding: 24px;
  background: ${scheme.colors.white};
  border-radius: 6px;
`
