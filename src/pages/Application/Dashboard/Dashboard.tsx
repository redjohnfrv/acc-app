import React, {useEffect} from 'react'
import styled from 'styled-components'
import {Loader} from '../../../ui/Loader'
import {Header} from '../components/Header'
import {Container} from '../ui'
import {Title} from '../ui'
import {ProspectsList} from '../components/ProspectsList'
import {FavouritesList} from '../components/FavouritesList'
import {Reports} from '../components/Reports'
import {NavLink} from 'react-router-dom'
import {mediaMedium, scheme} from '../../../assets/styles'
import {ESort, TO_PROSPECTS} from '../../../constants'
import {useAppDispatch, useAppSelector} from '../../../redux/hooks'
import {savedListActions, savedListSelectors} from '../../../redux/savedList'
import {companiesActions, companiesSelectors} from '../../../redux/companies'
import {teamActions, teamSelectors} from '../../../redux/team'

export const Dashboard = () => {
  const dispatch = useAppDispatch()

  const prospects = useAppSelector(savedListSelectors.getSavedLists)
  const isProspectsLoading = useAppSelector(savedListSelectors.isLoading)

  const favouriteCompanies = useAppSelector(companiesSelectors.getFavourites)
  const isFavouritesLoading = useAppSelector(companiesSelectors.isLoading)

  const team = useAppSelector(teamSelectors.getTeam)
  const isTeamLoading = useAppSelector(teamSelectors.isLoading)

  useEffect(() => {
    dispatch(teamActions.getReports())
    dispatch(savedListActions.getSavedLists({page: 1, limit: 2, sort: ESort.Alphabet}))
    dispatch(companiesActions.getFavouriteCompanies({page: 1, limit: 6}))
  }, [])

  return (
    <>
      <Header />
      <Container>
        <Title title="Dashboard" />
        {isProspectsLoading || isFavouritesLoading || isTeamLoading
          ? <Loader />
          : <Wrapper>
              <ProspectsWrapper>
                <ProspectTitleWrapper>
                  <ContentTitle>Prospect Session</ContentTitle>
                  {prospects.length > 0 && <NavLink to={TO_PROSPECTS}>see more</NavLink>}
                </ProspectTitleWrapper>
                <ProspectsList prospects={prospects} />
              </ProspectsWrapper>
              <SubInfoWrapper>
                <FavouritesList favourites={favouriteCompanies}/>
                <Reports
                  searchSessions={team?.searchCount}
                  sentPitches={team?.pitchCount}
                  hasReports={!!team}
                />
              </SubInfoWrapper>
            </Wrapper>
        }
      </Container>
    </>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 32px;
`
const ContentTitle = styled.h2`
  margin-bottom: 16px;
  font-family: 'Rubik-Medium', sans-serif;
  font-size: ${scheme.size.titleH2};
  color: ${scheme.colors.black};
`
const ProspectsWrapper = styled.div`
  margin-bottom: 40px;
`
const ProspectTitleWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  
  & a {
    font-size: ${scheme.size.textSmall};
    
    &:hover {
      text-decoration: underline;
    }
  }
`
const SubInfoWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 24px;
  
  @media (max-width: ${mediaMedium}) {
    grid-template-columns: repeat(1, 1fr);
  }
`
