import React from 'react'
import styled from 'styled-components'
import {HeaderSubMenu} from '../HeaderSubMenu'
import {mediaMedium, scheme} from '../../../../assets/styles'
import {images} from '../../../../assets/images'
import {NO_NAME, TO_DASHBOARD, TO_PROSPECTS, TO_SEARCH} from '../../../../constants'
import {Link, NavLink, useLocation} from 'react-router-dom'
import {useAppSelector} from '../../../../redux/hooks'
import {userSelectors} from '../../../../redux/user'
import {MobileMenu} from '../MobileMenu'
import {useSwitcher} from '../../../../hooks'

export const Header = () => {

  const {pathname} = useLocation()
  const {firstName} = useAppSelector(userSelectors.getUserData)

  const showMenu = useSwitcher()

  const closeMenu = () => {
    showMenu.off()
    document.body.style.overflow = 'auto'
  }
  const openMenu = () => {
    showMenu.on()
    document.body.style.overflow = 'hidden'
  }

  return (
    <>
      <MobileMenu closeMenu={closeMenu} show={showMenu.isOn} name={firstName || NO_NAME} />
      <Wrapper>
        <Logo to={TO_DASHBOARD}>
          <img src={images.pics.logoDark} alt="accelerist" />
          <span>accelerist</span>
        </Logo>
        <MenuList>
          <MenuItem
            $active={pathname === TO_DASHBOARD}
            to={TO_DASHBOARD}
          >Dashboard</MenuItem>
          <MenuItem
            $active={pathname === TO_PROSPECTS}
            to={TO_PROSPECTS}
          >Prospects</MenuItem>
          <MenuItem
            $active={pathname === TO_SEARCH}
            to={TO_SEARCH}
          >Search</MenuItem>
        </MenuList>
        <UserMenuWrapper>
          <UserMenu>
            <img src={images.icons.noImage} alt="user"/>
          </UserMenu>
          <UserTitle>{firstName ?? NO_NAME}</UserTitle>
          <HeaderSubMenu />
        </UserMenuWrapper>
        <ShowMobileBtn onClick={openMenu} />
      </Wrapper>
    </>
  )
}

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  height: 80px;
  padding: 0 60px;
  background: ${scheme.colors.commonBlue};
  
  @media (max-width: ${mediaMedium}) {
    padding: 0 20px;
    justify-content: space-between;
  }
`
const Logo = styled(Link)`
  display: flex;
  align-items: center;
  margin-right: 50px;
  
  @media (max-width: ${mediaMedium}) {
    margin-right: 20px;
  }
  
  & img {
    width: 42px;
    height: 42px;
    margin-right: 12px;
  }
  
  & span {
    font-family: 'Rubik-Medium', sans-serif;
    font-size: 16px;
    color: ${scheme.colors.black};
    text-transform: uppercase;
    letter-spacing: 2px;
    
    @media (max-width: ${mediaMedium}) {
      display: none;
    }
  }
`
const MenuList = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  
  @media (max-width: ${mediaMedium}) {
    display: none;
  }
`
const MenuItem = styled(NavLink)<{$active: boolean}>`
  position: relative;
  margin-right: 28px;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.black};
  
  &:last-child {
    margin-right: 0;
  }
  
  &:after {
    content: '';
    position: absolute;
    bottom: -8px;
    left: 0;
    width: 100%;
    height: 1px;
    background-color: ${scheme.colors.black};
    pointer-events: none;
    opacity: ${props => props.$active ? '1' : '0'};
  }
`
const UserMenuWrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
  justify-self: flex-end;
  padding: 10px 0;
  cursor: pointer;
  
  @media (max-width: ${mediaMedium}) {
    display: none;
  }
  
  &:hover {
    & div:last-child {
      display: block;
    } 
  }
`
const UserMenu = styled.div`
  width: 36px;
  height: 36px;
  margin-right: 12px;
  
  & img {
    width: 100%;
  }
`
const UserTitle = styled.span`
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.black};
  white-space: nowrap;
`
const ShowMobileBtn = styled.div`
  display: none;
  width: 36px;
  height: 36px;
  background: url(${images.icons.menu}) center / cover no-repeat;
  
  @media (max-width: ${mediaMedium}) {
    display: block;
  }
`
