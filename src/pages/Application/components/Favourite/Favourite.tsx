import React from 'react'
import styled from 'styled-components'
import {ContentBlock} from '../../ui'
import {NavLink} from 'react-router-dom'
import {images} from '../../../../assets/images'
import {scheme} from '../../../../assets/styles'

type Props = {
  id: string
  name: string
  employee?: number
  revenueRange?: string
  industry?: string[]
}

export const Favourite = ({...props}: Props) => {
  const {id, name, employee, revenueRange, industry} = props
  return (
    <ContentBlock>
      <Wrapper>
        <Header>
          <img src={images.pics.companyNoImage} alt="company" />
          <MainInfo>
            <Name to={`/company/${id}`}>{name}</Name>
            <Employee>Employees: {employee}</Employee>
          </MainInfo>
        </Header>
        <Content>
          <Revenue>Revenue Range: {revenueRange}</Revenue>
          <Industry>
            {industry &&
              industry.map((item, index) => {
                return (
                  <div key={index}>
                    <span key={index}>{item}</span>
                    {industry.length === index &&
                      <img src={images.icons.dot} alt="divider" />
                    }
                  </div>
                )
              })
            }
          </Industry>
        </Content>
      </Wrapper>
    </ContentBlock>
  )
}

const Wrapper = styled.div`
  
`
const Header = styled.div`
  display: flex;
  justify-content: flex-start;
  margin-bottom: 20px;
  
  & img {
    width: 48px;
    height: 48px;
    margin-right: 12px;
    border: 1px solid ${scheme.colors.lightBlue};
    border-radius: 6px;
  }
`
const MainInfo = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`
const Name = styled(NavLink)`
  font-family: 'Rubik-Medium', sans-serif;
  margin-bottom: 4px;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.black};
  line-height: 18px;
`
const Employee = styled.span`
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.darkGray};
  line-height: 18px;
`
const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`
const Revenue = styled.div`
  margin-bottom: 11px;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.darkGray};
  line-height: 18px;
`
const Industry = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 6px;
  
  & span {
    font-family: 'Rubik-Medium', sans-serif;
    margin-bottom: 4px;
    font-size: ${scheme.size.textSmall};
    color: ${scheme.colors.black};
    line-height: 18px;
  }
  
  & img {
    width: 4px;
    height: 4px;
  }
`
