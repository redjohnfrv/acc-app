import React from 'react'
import styled from 'styled-components'
import {scheme} from '../../../../assets/styles'
import {TO_DASHBOARD, TO_LOGIN, TO_PROSPECTS, TO_SEARCH} from '../../../../constants'
import {useAppDispatch} from '../../../../redux/hooks'
import {NavLink, useNavigate} from 'react-router-dom'
import {userActions} from '../../../../redux/user'

export const HeaderSubMenu = () => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const logoutHandler = () => {
    dispatch(userActions.logout())
    navigate(TO_LOGIN)
  }

  return (
    <SubMenu>
      <MenuItem to={TO_DASHBOARD}>Dashboard</MenuItem>
      <MenuItem to={TO_PROSPECTS}>Prospects</MenuItem>
      <MenuItem to={TO_SEARCH}>Search</MenuItem>
      <Logout onClick={logoutHandler}>Log out</Logout>
    </SubMenu>
  )
}

const SubMenu = styled.div`
  position: absolute;
  display: none;
  bottom: -120px;
  left: 0;
  height: 120px;
  min-width: 120px;
  padding: 12px;
  background: ${scheme.colors.white};
  box-shadow: 0 0 2px 1px ${scheme.colors.lightGray};
  border-radius: 6px;
  z-index: 10;
`
const Logout = styled.span`
  display: block;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.red};
  
  &:hover {
    text-decoration: underline;
  }
`
const MenuItem = styled(NavLink)`
  display: block;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.black};
  margin-bottom: 12px;
  
  &:hover {
    text-decoration: underline;
  }
`
