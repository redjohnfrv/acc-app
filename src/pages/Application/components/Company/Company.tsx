import React from 'react'
import styled from 'styled-components'
import {ContentBlock} from '../../ui'
import {CompanyAvatar, Csr} from './components'
import {images} from '../../../../assets/images'
import {mediaMedium, scheme} from '../../../../assets/styles'
import {CompanyType} from '../../../../redux/companies/types'

type Props = {
  company: CompanyType
  likeHandler: (id: string, isLiked: boolean) => void
}

export const Company = ({company, likeHandler}: Props) => {

  const {id, score, name, street, city, state, zipCode, phone, revenueRange, like} = company

  return (
    <ContentBlock>
      <CompanyPreview>
        <CompanyAvatar name={name} score={score} />
        <CompanyInfo>
          <CompanyTitle>{name}</CompanyTitle>
          <Address>{street}, {city}, {state}, {zipCode}</Address>
          <PhoneNumber>{phone}</PhoneNumber>
          <Features>
            <Csr />
            <Revenue>
              <RevenueTitle>Revenue</RevenueTitle>
              <RevenueValue>{revenueRange}</RevenueValue>
            </Revenue>
          </Features>
          <CompanyButtons>
            <LikeButton
              isLiked={like}
              onClick={() => likeHandler(id, like)}
            />
            <ProfileButton>Profile</ProfileButton>
          </CompanyButtons>
        </CompanyInfo>
      </CompanyPreview>
    </ContentBlock>
  )
}

const CompanyPreview = styled.div`
  display: flex;
  justify-content: space-between;
  gap: 16px;
  
  @media (max-width: ${mediaMedium}) {
    flex-direction: column;
  }
`
const CompanyInfo = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`
const CompanyTitle = styled.h3`
  margin-bottom: 12px;
  line-height: 24px;
  font-size: ${scheme.size.titleH3};
  color: ${scheme.colors.black};
`
const Address = styled.span`
  max-width: 290px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.darkGray};
  margin-bottom: 4px;
  
  @media (max-width: ${mediaMedium}) {
    white-space: pre-wrap;
    text-overflow: unset;
  }
`
const PhoneNumber = styled(Address)``
const Features = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 24px 0;
  border-bottom: 1px solid ${scheme.colors.lightGray};
`

const Revenue = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  padding-left: 21px;
`
const RevenueTitle = styled(Address)``
const RevenueValue = styled.span`
  font-family: 'Rubik-Medium', sans-serif;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.black};
  
  @media (max-width: ${mediaMedium}) {
    text-align: right;
  }
`
const CompanyButtons = styled.div`
  display: flex;
  justify-content: space-between;
  gap: 8px;
  width: 100%;
`
const LikeButton = styled.div<{isLiked: boolean}>`
  width: 36px;
  height: 36px;
  background: url(${props => props.isLiked ? images.pics.heartFull : images.pics.heart}) center / 24px no-repeat;
  border: 1px solid ${scheme.colors.lightGray};
  border-radius: 6px;
  cursor: pointer;
`
const ProfileButton = styled.div`
  width: calc(100% - 44px);
  height: 36px;
  line-height: 36px;
  text-align: center;
  font-size: ${scheme.size.textSmall};
  border: 1px solid ${scheme.colors.blue};
  border-radius: 6px;
  cursor: pointer;
`
