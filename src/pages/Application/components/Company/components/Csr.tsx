import React from 'react'
import styled from 'styled-components'
import {images} from '../../../../../assets/images'
import {scheme} from '../../../../../assets/styles'

export const Csr = () => {
  return (
    <CsrFocus>
      <CsrTitle>CSR Focus</CsrTitle>
      <CsrList>
        <span>Health</span>
        <img src={images.icons.dot} alt="divider" />
        <span>Animals</span>
        <img src={images.icons.dot} alt="divider" />
        <span>Education</span>
      </CsrList>
    </CsrFocus>
  )
}

const CsrFocus = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 4px;
  padding-right: 21px;
  border-right: 1px solid ${scheme.colors.lightGray};
`
const CsrTitle = styled.h3`
  margin-bottom: 12px;
  line-height: 24px;
  font-size: ${scheme.size.titleH3};
  color: ${scheme.colors.black};
`
const CsrList = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  gap: 6px;
  margin-bottom: 12px;
  
  & span {
    font-family: 'Rubik-Medium', sans-serif;
    font-size: ${scheme.size.textSmall};
    color: ${scheme.colors.black};
  }
`
