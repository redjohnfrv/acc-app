import React from 'react'
import styled from 'styled-components'
import {images} from '../../../../../assets/images'
import {mediaMedium, scheme} from '../../../../../assets/styles'

type Props = {
  name: string
  score: number
}

export const CompanyAvatar = ({name, score}: Props) => {
  return (
    <Wrapper>
      <Avatar src={images.pics.companyNoImage} alt={name} />
      <Ranking>
        <RankingTitle>Priority Ranking</RankingTitle>
        <RankingValue>{score}</RankingValue>
      </Ranking>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid ${scheme.colors.lightGray};
  border-radius: 6px;
  
  @media (max-width: ${mediaMedium}) {
    align-items: center;
  }
`
const Avatar = styled.img`
  width: 166px;
  height: 155px;
  margin-bottom: 8px;
  border-bottom: 1px solid ${scheme.colors.lightGray};
`
const Ranking = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`
const RankingTitle = styled.span`
  margin-bottom: 2px;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.darkGray};
`
const RankingValue = styled.span`
  margin-bottom: 8px;
  font-family: 'Rubik-Medium', sans-serif;
  font-size: ${scheme.size.textNormal};
  color: ${scheme.colors.black};
`
