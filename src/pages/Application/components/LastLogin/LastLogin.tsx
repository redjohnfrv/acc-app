import React from 'react'
import styled from 'styled-components'
import {scheme} from '../../../../assets/styles'
import {images} from '../../../../assets/images'
import {LastLoginType} from '../../../../redux/team/types'
import {dateFormat} from '../../../../helpers'
import { NO_NAME } from '../../../../constants'

type Props = {
  lastLogin: LastLoginType
}

export const LastLogin = ({lastLogin}: Props) => {

  const {day, monthByName, fullYear} = dateFormat(Date.parse(lastLogin.loggedInAt))

  return (
    <Wrapper>
      <Member>
        <img src={images.pics.avatar} alt="member" />
        <span>{lastLogin.user.firstName ?? NO_NAME}</span>
      </Member>
      <LoginDate>
        {day} {monthByName} {fullYear}
      </LoginDate>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  margin-bottom: 8px;
  
  &:after {
    content: '';
    position: absolute;
    bottom: -8px;
    left: 42px;
    width: calc(100% - 42px);
    height: 1px;
    background: #EEE;
  }
`
const Member = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  
  & img {
    width: 32px;
    height: 32px;
    margin-right: 10px;
    border-radius: 50%;
    border: 1px solid ${scheme.colors.lightGray};
  }
  
  & span {
    font-family: 'Rubik-Medium', sans-serif;
    font-size: ${scheme.size.textSmall};
    color: ${scheme.colors.black};
  }
`
const LoginDate = styled.span`
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.darkGray};
`
