import React from 'react'
import styled from 'styled-components'
import {Favourite} from '../Favourite'
import {ContentBlock} from '../../ui'
import {mediaMedium, scheme} from '../../../../assets/styles'
import {CompanyType} from '../../../../redux/companies/types'

type Props = {
  favourites: CompanyType[]
}

export const FavouritesList = ({favourites}: Props) => {
  return (
    <Wrapper>
      <Title>Favourites</Title>
      <List>
        {favourites.length > 0
          ? favourites.map(item => <Favourite
            key={item.id}
            id={item.id}
            name={item.name}
            employee={item.employeeCount}
            revenueRange={item.revenueRange}
            industry={item.primaryIndustry}
          />)
          : <NoFavourites>
              <ContentBlock>
                <span>No Favourites Companies ...</span>
              </ContentBlock>
            </NoFavourites>
        }
      </List>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`
const Title = styled.h2`
  margin-bottom: 16px;
  font-family: 'Rubik-Medium', sans-serif;
  font-size: ${scheme.size.titleH2};
  color: ${scheme.colors.black};
`
const List = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 24px;

  @media (max-width: ${mediaMedium}) {
    grid-template-columns: repeat(1, 1fr);
  }
`
const NoFavourites = styled.div`
  
`
