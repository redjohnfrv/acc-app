import React from 'react'
import styled from 'styled-components'
import {Company} from '../Company/Company'
import {CompanyType} from '../../../../redux/companies/types'
import {useAppDispatch} from '../../../../redux/hooks'
import {companiesActions} from '../../../../redux/companies'
import { toast } from 'react-toastify'
import {mediaMedium} from '../../../../assets/styles'

type Props = {
  companies: CompanyType[]
}

export const CompaniesList = ({companies}: Props) => {
  const dispatch = useAppDispatch()

  const likeHandler = (id: string, isLiked: boolean) => isLiked
    ? (dispatch(companiesActions.dislikeCompany(id)),
      toast.success('Company successfully unliked!'))
    : (dispatch(companiesActions.likeCompany(id)),
      toast.success('Company successfully liked!'))

  return (
    <Wrapper>
      {companies.map(item => <Company key={item.id} company={item} likeHandler={likeHandler} />)}
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 24px;
  
  @media (max-width: ${mediaMedium}) {
    grid-template-columns: repeat(1, 1fr);
  }
`
