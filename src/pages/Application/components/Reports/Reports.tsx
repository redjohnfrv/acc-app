import React from 'react'
import styled from 'styled-components'
import {ContentBlock} from '../../ui'
import {LastLogin} from '../LastLogin'
import {mediaMedium, scheme} from '../../../../assets/styles'
import {images} from '../../../../assets/images'
import {useAppSelector} from '../../../../redux/hooks'
import {teamSelectors} from '../../../../redux/team'
import {LastLoginType} from '../../../../redux/team/types'

type Props = {
  searchSessions?: number
  sentPitches?: number
  hasReports: boolean
}

export const Reports = ({searchSessions, sentPitches, hasReports}: Props) => {

  const lastLogins = useAppSelector(teamSelectors.getLastLogins)

  return (
    <Wrapper>
      <Title>Reports</Title>
      {hasReports
        ? <ContentBlock>
            <Session>
              <LineWrapper>
                <ReportsTitle>Search Sessions</ReportsTitle>
                <BlockCount>
                  <BlockCountTitle>Total</BlockCountTitle>
                  <BlockCountValue>{searchSessions}</BlockCountValue>
                </BlockCount>
              </LineWrapper>
              <LineWrapper>
                <ReportsTitle>Sent Pitches</ReportsTitle>
                <BlockCount>
                  <BlockCountTitle>Company</BlockCountTitle>
                  <BlockCountValue>{sentPitches}</BlockCountValue>
                </BlockCount>
              </LineWrapper>
            </Session>
            <TopMatched>
              <LineWrapper>
                <ReportsTitle>Top Matched</ReportsTitle>
                <CompaniesLogos>
                  <img src={images.pics.compLogo01} alt="company logo" />
                  <img src={images.pics.compLogo02} alt="company logo" />
                  <img src={images.pics.compLogo03} alt="company logo" />
                  <img src={images.pics.compLogo04} alt="company logo" />
                </CompaniesLogos>
              </LineWrapper>
            </TopMatched>
            <LastLoginList>
              <ReportsTitle>Last Login</ReportsTitle>
              {lastLogins
                && lastLogins.map((item: LastLoginType) =>
                  <LastLogin
                    key={item.id}
                    lastLogin={item}
                  />)}
            </LastLoginList>
          </ContentBlock>
        : <NoReports>
            <ContentBlock>
              No Reports ...
            </ContentBlock>
          </NoReports>
      }
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`
const Title = styled.h2`
  margin-bottom: 16px;
  font-family: 'Rubik-Medium', sans-serif;
  font-size: ${scheme.size.titleH2};
  color: ${scheme.colors.black};
`
const NoReports = styled.div`
  width: 100%;
  
  & div {
    width: 100%;
    min-height: 156px;
  }
`
const Session = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 24px;

  @media (max-width: ${mediaMedium}) {
    flex-direction: column;
  }
`
const LineWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  @media (max-width: ${mediaMedium}) {
   align-items: center;
   
    &:first-child {
      margin-bottom: 24px;
    }
  }
  
  &:first-child {
    margin-right: 18px;
  }
`
const ReportsTitle = styled.h3`
  font-family: 'Rubik-Medium', sans-serif;
  margin-bottom: 16px;
`
const BlockCount = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  padding: 5px 0;
  background: ${scheme.colors.whiteGray};
  border-radius: 6px;
`
const BlockCountTitle = styled.span`
  margin-bottom: 8px;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.darkGray};
`
const BlockCountValue = styled.span`
  font-family: 'Rubik-Medium', sans-serif;
  font-size: ${scheme.size.textBig};
  line-height: 35px;
  color: ${scheme.colors.black};
`
const TopMatched = styled.div`
  margin-bottom: 24px;
`
const CompaniesLogos = styled.div`
  display: flex;
  justify-content: flex-start;
  gap: 12px;

  @media (max-width: ${mediaMedium}) {
    flex-wrap: wrap;
  }
  
  & img {
    width: 83px;
    height: 83px;
    border-radius: 6px;
  }
`
const LastLoginList = styled.div`
  
`
