import React from 'react'
import styled from 'styled-components'
import {ContentBlock} from '../../ui'
import {Available} from './components/Available'
import {Filters} from './components/Filters'
import {Author} from './components/Author'
import {LastActivity} from './components/LastActivity'
import {NavLink} from 'react-router-dom'
import {mediaMedium, scheme} from '../../../../assets/styles'
import {NO_NAME, TO_PROSPECTS} from '../../../../constants'
import {ProspectsType} from '../../../../redux/savedList/types'
import {isObjEmpty} from '../../../../helpers'

type Props = {
  prospect: ProspectsType
}

export const ProspectItem = ({prospect}: Props) => {
  const {id, createdAt, filters, lastAuthor, name, prospectsAvailable, updatedAt} = prospect

  const filteredFilters = filters && !isObjEmpty(filters)
    ? Object.entries(filters).filter(item => item[0] !== 'deleteIds').slice(0, 5)
    : []

  return (
    <Wrapper>
      <ContentBlock>
        <NavLink to={`${TO_PROSPECTS}/${id}`}>
          <Title>{name ?? NO_NAME}</Title>
        </NavLink>
        {filteredFilters.length > 0
          ? <Filters filteredFilters={filteredFilters} />
          : null
        }
        <AvailableWrapper>
          <Available title="№ of Prospects Available:" prospectsAvailable={prospectsAvailable} />
          <Available title="№ of Contacts Available:" prospectsAvailable={0} />
        </AvailableWrapper>
        <AuthorInfo>
          <Author lastAuthor={lastAuthor} />
          <LastActivity date={updatedAt ?? createdAt} />
        </AuthorInfo>
      </ContentBlock>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  
`
const Title = styled.h2`
  position: relative;
  width: 100%;
  padding-bottom: 9px;
  margin-bottom: 16px;
  font-size: ${scheme.size.titleH3};
  color: ${scheme.colors.black};
  
  &:after {
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 1px;
    background: ${scheme.colors.lightGray};
  }
`
const AvailableWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  gap: 18px;
  width: 100%;
  margin: 24px 0;

  @media (max-width: ${mediaMedium}) {
    flex-direction: column;
  }
`
const AuthorInfo = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`

