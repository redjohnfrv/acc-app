import React from 'react'
import styled from 'styled-components'
import {scheme} from '../../../../../../assets/styles'

type Props = {
  prospectsAvailable: number
  title: string
}

export const Available = ({prospectsAvailable, title}: Props) => {
  return (
    <AvailableItem>
      <span>{title}</span>
      <span>{prospectsAvailable}</span>
    </AvailableItem>
  )
}

const AvailableItem = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  padding: 5px 6px 15px 6px;
  background: ${scheme.colors.whiteGray};
  border-radius: 6px;
  
  & span:not(:last-child) {
    margin-bottom: 12px;
    font-size: ${scheme.size.textSmall};
    color: ${scheme.colors.darkGray};
  }
  
  & span:last-child {
    font-family: 'Rubik-Medium', sans-serif;
    font-size: ${scheme.size.titleH2};
    color: ${scheme.colors.black};
  }
`
