import React from 'react'
import styled from 'styled-components'
import {images} from '../../../../../../assets/images'
import {scheme} from '../../../../../../assets/styles'
import {UserType} from '../../../../../../redux/user/types'
import {NO_NAME} from '../../../../../../constants'

type Props = {
  lastAuthor: UserType
}

export const Author = ({lastAuthor}: Props) => {
  const {firstName, lastName, role} = lastAuthor
  return (
    <Wrapper>
      <Avatar />
      <Info>
        <FullName>{firstName && lastName ? `${firstName} ${lastName}` : NO_NAME}</FullName>
        <Role>{role}</Role>
      </Info>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 12px;
`
const Avatar = styled.div`
  width: 40px;
  height: 40px;
  background: url(${images.pics.avatar}) top / cover no-repeat;
  border: 1px solid ${scheme.colors.lightGray};
  border-radius: 50%;
`
const Info = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`
const FullName = styled.span`
  font-family: 'Rubik-Medium', sans-serif;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.black};
`
const Role = styled.span`
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.darkGray};
`
