import React from 'react'
import styled from 'styled-components'
import {FilterBox} from '../../../../ui'
import {scheme} from '../../../../../../assets/styles'
import {EMPTY_STRING} from '../../../../../../constants'

type Props = {
  filteredFilters: [string, string | string[] | number[]][]
}

export const Filters = ({filteredFilters}: Props) => {

  return (
    <Wrapper>
      <FiltersTitle>Filters:</FiltersTitle>
      <FilterList>
        {filteredFilters.map((item: [string, string | string[] | number[]], index: number) => {
          return <FilterBox key={index}>
                  {item[0]}
                  {Array.isArray(item[1])
                    ? EMPTY_STRING
                    : `: ${item[1]}`
                  }
                </FilterBox>
        })}
      </FilterList>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`
const FiltersTitle = styled.span`
  margin-bottom: 12px;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.darkGray};
`
const FilterList = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;
  gap: 8px;
`
