import React from 'react'
import styled from 'styled-components'
import {scheme} from '../../../../../../assets/styles'
import {dateFormat} from '../../../../../../helpers'

type Props = {
  date: string
}

export const LastActivity = ({date}: Props) => {

  const {day, monthByName, fullYear} = dateFormat(Date.parse(date))

  return (
    <Wrapper>
      <Title>Last activity</Title>
      <Activity>{day} {monthByName} {fullYear}</Activity>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`
const Title = styled.span`
  margin-bottom: 4px;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.darkGray};
`
const Activity = styled.span`
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.black};
`


