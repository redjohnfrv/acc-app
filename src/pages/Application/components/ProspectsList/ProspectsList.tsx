import React from 'react'
import styled from 'styled-components'
import {ProspectItem} from '../ProspectItem'
import {ContentBlock} from '../../ui'
import {mediaMedium} from '../../../../assets/styles'
import {ProspectsType} from '../../../../redux/savedList/types'

type Props = {
  prospects: ProspectsType[]
}

export const ProspectsList = ({prospects}: Props) => {
  return (
    <Wrapper>
      {prospects.length > 0
        ? prospects.map(prospect => {
          return <ProspectItem key={prospect.id} prospect={prospect} />
        })
        : <NoProspects>
            <ContentBlock>
              <span>Your session is empty...</span>
            </ContentBlock>
          </NoProspects>
      }
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 24px;

  @media (max-width: ${mediaMedium}) {
    grid-template-columns: repeat(1, 1fr);
  }
`
const NoProspects = styled.div`

`
