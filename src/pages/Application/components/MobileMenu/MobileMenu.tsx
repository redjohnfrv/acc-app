import React from 'react'
import styled from 'styled-components'
import { images } from '../../../../assets/images'
import {scheme} from '../../../../assets/styles'
import {Link, useNavigate} from 'react-router-dom'
import {TO_DASHBOARD, TO_LOGIN, TO_PROSPECTS, TO_SEARCH} from '../../../../constants'
import {useAppDispatch} from '../../../../redux/hooks'
import {userActions} from '../../../../redux/user'

type Props = {
  closeMenu: () => void
  show: boolean
  name: string
}

export const MobileMenu = ({closeMenu, show, name}: Props) => {

  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const logoutHandler = () => {
    dispatch(userActions.logout())
    navigate(TO_LOGIN)
    closeMenu()
  }

  return (
    <Wrapper show={show}>
      <CloseMenu onClick={closeMenu} />
      <MenuList>
        <MenuItem onClick={closeMenu} to={TO_DASHBOARD}>Dashboard</MenuItem>
        <MenuItem onClick={closeMenu} to={TO_PROSPECTS}>Prospects</MenuItem>
        <MenuItem onClick={closeMenu} to={TO_SEARCH}>Search</MenuItem>
        <Logout onClick={logoutHandler}>Logout</Logout>
      </MenuList>
      <User>
        <img src={images.icons.noImage} alt="user" />
        <span>{name}</span>
      </User>
    </Wrapper>
  )
}

const Wrapper = styled.div<{show: boolean}>`
  position: fixed;
  top: 0;
  left: ${props => props.show ? '0' : '100%'};
  width: ${props => props.show ? '100%' : '0'};
  height: 100vh;
  background: ${scheme.colors.white};
  opacity: ${props => props.show ? '1' : '0'};
  transition: all .3s ease;
  overflow: hidden;
  z-index: 10;
 `
const CloseMenu = styled.div`
  position: absolute;
  left: calc(100% - 44px);
  top: 20px;
  width: 24px;
  height: 24px;
  background: url(${images.icons.closeMenu}) center / cover no-repeat;
`
const MenuList = styled.div`
  display: flex;
  flex-direction: column;
  gap: 32px;
  padding: 129px 0 0 40px;
`
const MenuItem = styled(Link)`
  font-size: ${scheme.size.textNormal};
  color: ${scheme.colors.black};
  
  &:first-child {
    font-family: 'Rubik-Medium', sans-serif;
  }
`
const User = styled.div`
  position: absolute;
  left: 20px;
  bottom: 20px;
  display: flex;
  align-items: center;
  gap: 20px;
  
  & img {
    width: 50px;
    height: 50px;
  }
  
  & span {
    font-size: ${scheme.size.textNormal};
    color: ${scheme.colors.black};
  }
`
const Logout = styled.span`
  font-size: ${scheme.size.textNormal};
  color: ${scheme.colors.red};
`
