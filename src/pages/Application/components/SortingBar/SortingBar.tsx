import React from 'react'
import styled from 'styled-components'
import {mediaMedium, scheme} from '../../../../assets/styles'
import {ESort} from '../../../../constants'

type Props = {
  sorting: ESort
  sortingHandler: (type: ESort) => void
}

export const SortingBar = ({sorting, sortingHandler}: Props) => {
  return (
    <Wrapper>
      <SortingTitle>Sort by:</SortingTitle>
      <SortingMenu>
        <SortingMenuItem
          $active={sorting === ESort.Alphabet}
          onClick={() => sortingHandler(ESort.Alphabet)}
        >Alphabet</SortingMenuItem>
        <SortingMenuItem
          $active={sorting === ESort.Available}
          onClick={() => sortingHandler(ESort.Available)}
        >Prospects Available</SortingMenuItem>
        <SortingMenuItem
          $active={sorting === ESort.LastActivity}
          onClick={() => sortingHandler(ESort.LastActivity)}
        >Last Activity</SortingMenuItem>
      </SortingMenu>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;

  @media (max-width: ${mediaMedium}) {
    display: flex;
    align-items: flex-start;
    flex-direction: column;
    gap: 16px;
  }
`
const SortingTitle = styled.span`
  margin-right: 26px;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.darkGray};

  @media (max-width: ${mediaMedium}) {
    margin-right: 0;
  }
`
const SortingMenu = styled.nav`
  display: flex;
  justify-content: flex-start;
  gap: 22px;

  @media (max-width: ${mediaMedium}) {
    width: max-content;
    margin-bottom: 24px;
  }
`
const SortingMenuItem = styled.span<{$active: boolean}>`
  position: relative;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.black};
  cursor: pointer;
  border-bottom: ${props => props.$active ? `2px solid ${scheme.colors.blue}` : 'none'};
`

