import React, {useEffect} from 'react'
import styled from 'styled-components'
import {Loader} from '../../../ui/Loader'
import {Header} from '../components/Header'
import {Container} from '../ui'
import {ProspectsList} from '../components/ProspectsList'
import {SortingBar} from '../components/SortingBar'
import {Title} from '../ui'
import {Pagination} from '../../../ui/Pagination'
import {mediaMedium} from '../../../assets/styles'
import {ESort} from '../../../constants'
import {savedListActions, savedListSelectors} from '../../../redux/savedList'
import {useAppDispatch, useAppSelector} from '../../../redux/hooks'
import {useLocation, useNavigate} from 'react-router-dom'
import queryString from 'query-string'

export const Prospects = () => {

  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const {pathname, search} = useLocation()

  const prospects = useAppSelector(savedListSelectors.getSavedLists)
  const meta = useAppSelector(savedListSelectors.getMeta)
  const isProspectsLoading = useAppSelector(savedListSelectors.isLoading)

  const {page} = queryString.parse(search)
  const [sorting, setSorting] = React.useState(ESort.Alphabet)

  useEffect(() => {
    dispatch(savedListActions.getSavedLists({page: Number(page) || 1, limit: 12, sort: sorting}))
  }, [page, sorting])

  const sortingHandler = (sort: ESort) => {
    setSorting(sort)
    const params = {...queryString.parse(search), sort}
    navigate(`${pathname}?${queryString.stringify(params)}`)
  }

  const paginationHandler = (page: number) => {
    if (page > 0 && page <= meta.totalPages) {
      const params = {...queryString.parse(search), page}
      dispatch(savedListActions.getSavedLists({page: page, limit: 12, sort: sorting}))
      navigate(`${pathname}?${queryString.stringify(params)}`)
    }
  }

  return (
    <>
      <Header />
      <Container>
        <Title title="Prospects" />
        <Wrapper>
          {isProspectsLoading
            ? <Loader />
            : <ProspectsWrapper>
                <NavBar>
                  <SortingBar sorting={sorting} sortingHandler={sortingHandler} />
                  {meta &&
                    <Pagination
                      items={prospects.length}
                      currentPage={Number(meta.currentPage)}
                      itemsPerPage={Number(meta.itemsPerPage)}
                      totalItems={meta.totalItems}
                      totalPages={meta.totalPages}
                      paginationHandler={paginationHandler}
                    />
                  }
                </NavBar>
                <ProspectsList prospects={prospects} />
              </ProspectsWrapper>
          }
        </Wrapper>
      </Container>
    </>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 32px;
`
const ProspectsWrapper = styled.div`
  display: flex;
  flex-direction: column;
`
const NavBar = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 27px;

  @media (max-width: ${mediaMedium}) {
    flex-direction: column;
    align-items: flex-start;
    margin-bottom: 12px;
  }
`
