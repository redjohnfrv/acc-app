import React from 'react'
import {Navigate, Route, Routes} from 'react-router-dom'
import {Login} from './Login'
import {Register} from './Register'
import {ResetPassword} from './ResetPassword'
import {ResendLink} from './ResendLink'
import {
  TO_LOGIN,
  TO_NEW_PASSWORD,
  TO_REGISTER,
  TO_RESEND_LINK,
  TO_RESET,
  TO_ROOT
} from '../../constants'
import {NewPassword} from './NewPassword'

export const NavigatorAuth = () => {
  return (
    <Routes>
      <Route path={TO_LOGIN} element={<Login />} />
      <Route path={TO_REGISTER} element={<Register />} />
      <Route path={TO_RESET} element={<ResetPassword />} />
      <Route path={TO_RESEND_LINK} element={<ResendLink />} />
      <Route path={TO_NEW_PASSWORD} element={<NewPassword />} />
      <Route path={TO_ROOT} element={<Navigate replace to={TO_REGISTER} />} />
    </Routes>
  )
}
