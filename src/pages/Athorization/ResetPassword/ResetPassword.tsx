import React from 'react'
import {Container} from '../ui'
import {ResetForm} from '../components/Forms'
import {ReturnToLogin} from '../components/ReturnToLogin'

export const ResetPassword = () => {
  return (
    <Container>
      <ResetForm />
      <ReturnToLogin />
    </Container>
  )
}
