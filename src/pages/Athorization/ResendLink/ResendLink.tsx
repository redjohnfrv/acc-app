import React from 'react'
import {Container} from '../ui'
import {ResendLinkForm} from '../components/Forms'
import {ReturnToLogin} from '../components/ReturnToLogin'

export const ResendLink = () => {
  return (
    <Container>
      <ResendLinkForm />
      <ReturnToLogin />
    </Container>
  )
}
