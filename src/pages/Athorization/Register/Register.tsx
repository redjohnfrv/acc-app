import React from 'react'
import {Container} from '../ui'
import {RegisterForm} from '../components/Forms'

export const Register = () => {
  return (
    <Container>
      <RegisterForm />
    </Container>
  )
}
