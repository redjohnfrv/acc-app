import React from 'react'
import styled from 'styled-components'
import {Link} from 'react-router-dom'
import {scheme} from '../../../../assets/styles'
import {TIME, TO_LOGIN} from '../../../../constants'

export const ReturnToLogin = () => {

  const onClick = () => {
    setTimeout(() => sessionStorage.removeItem(TIME), 2000)
  }

  return (
    <LinkWrapper onClick={onClick}>
      <Link to={TO_LOGIN}>Return to login</Link>
    </LinkWrapper>
  )
}

const LinkWrapper = styled.div`
  position: absolute;
  display: flex;
  justify-content: center;
  bottom: 28px;
  width: 100%;
  
  & a {
    display: block;
    width: max-content;
    height: 36px;
    line-height: 36px;
    padding: 0 24px;
    color: ${scheme.colors.white};
    background: rgba(18, 36, 52, 0.15);
    border-radius: 6px;
  }
`
