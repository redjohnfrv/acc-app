import React from 'react'
import styled from 'styled-components'
import {Field, Form } from 'react-final-form'
import {FormContainer} from './FormContainer'
import {Input} from './FormInputs'
import {InputPwd} from './FormInputs'
import {PrimaryButton} from '../../../../ui/Buttons'
import {Link} from 'react-router-dom'
import {BLANK, SUBMIT, TO_LOGIN} from '../../../../constants'
import {mediaMedium, scheme} from '../../../../assets/styles'
import {images} from '../../../../assets/images'
import {validators, composeValidators} from '../../../../helpers'
import {useAppDispatch, useAppSelector} from '../../../../redux/hooks'
import {userActions, userSelectors} from '../../../../redux/user'

type FormValues = {
  email: string
  password: string
  remember: boolean
}

export const RegisterForm = () => {
  const dispatch = useAppDispatch()
  const isLoading = useAppSelector(userSelectors.isLoading)

  const onSubmit = (values: FormValues) => {
    const payload = {
      email: values.email,
      password: values.password,
    }
    dispatch(userActions.signupUser(payload))
  }

  return (
    <FormContainer>
      <Form
        onSubmit={onSubmit}
        render={({handleSubmit, submitting, pristine, invalid}) => (
          <form onSubmit={handleSubmit}>
            <Title>Welcome to Accelerist</Title>
            <Tabs>
              <Tab data-active to={BLANK}>
                Register
              </Tab>
              <Tab to={TO_LOGIN}>
                Login
              </Tab>
            </Tabs>
            <Field
              name="email"
              label="Email"
              placeholder="Enter email ..."
              component={Input}
              validate={composeValidators(validators.required, validators.validEmail)}
            />
            <Field
              name="password"
              label="Password"
              placeholder="Enter password"
              component={InputPwd}
              validate={composeValidators(validators.required, validators.passwordLength)}
            />
            <Terms>
              I agree that by clicking
              <strong> “Registration”</strong> I accept the
              <strong> Terms Of Service</strong> and
              <strong> Privacy Policy</strong>
            </Terms>
            <PrimaryButton
              text="Registration"
              type={SUBMIT}
              disabled={submitting || pristine || invalid}
              loading={isLoading}
            />
            <Continue>
              <span>or continue with</span>
              <a href={BLANK}><img src={images.icons.social} alt="social" /></a>
            </Continue>
          </form>
        )}
      />
    </FormContainer>
  )
}

const Title = styled.h2`
    margin-bottom: 25px;
`
const Tabs = styled.div`
  display: flex;
  width: 100%;
  margin-bottom: 32px;
  padding: 2px;
  background: ${scheme.colors.lightGray};
  border-radius: 6px;
  cursor: pointer;
`
const Tab = styled(Link)<{'data-active'?: boolean}>`
  flex: 1;
  height: 40px;
  line-height: 40px;
  background: ${props => props['data-active'] ? scheme.colors.lightBlue : scheme.colors.lightGray};
  text-align: center;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.darkGray};
  border-radius: 6px;
`
const Terms = styled.span`
  margin-top: 20px;
  margin-bottom: 32px;
  font-family: 'Rubik-Light', sans-serif;
  font-size: ${scheme.size.textSmall};
  text-align: center;

  @media (max-width: ${mediaMedium}) {
    margin-top: 10px;
    margin-bottom: 10px;
  }
`
const Continue = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  
  & span {
    font-size: ${scheme.size.textSmall};
    color: ${scheme.colors.darkGray};
    margin: 32px 0 24px 0;
  }
  
  & img {
    width: 44px;
    height: 44px;
  }
`
