import React from 'react'
import styled, {css} from 'styled-components'
import {mediaMedium, scheme} from '../../../../../assets/styles'
import {FieldRenderProps} from 'react-final-form'

type InputFiledProps = {
  placeholder?: string
  label?: string
  disabled?: boolean
}

type Props = FieldRenderProps<string> & InputFiledProps

export const Input = ({input, meta, placeholder, label, disabled}: Props) => {
  return (
    <InputWrapper>
      {label && <Label>{label}</Label>}
      <TextInput
        {...input}
        error={meta.error && meta.touched}
        disabled={disabled}
        placeholder={placeholder}
      />
      {meta.error && meta.touched && <Warning>{meta.error}</Warning>}
    </InputWrapper>
  )
}

const InputWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 100%;
  margin-bottom: 24px;

  @media (max-width: ${mediaMedium}) {
    margin-bottom: 20px;
  }
`
const Label = styled.label`
  margin-bottom: 4px;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.darkGray};
`
const TextInput = styled.input<{error: any}>`
  width: 100%;
  height: 46px;
  padding: 0 15px;
  border: 1px solid ${scheme.colors.lightGray};
  border-radius: 6px;

  @media (max-width: ${mediaMedium}) {
    font-size: ${scheme.size.textNormal};
  }
  
  ${props => props.error && css`
    background-color: ${scheme.colors.white};
    border: 1px solid ${scheme.colors.red};
    
    &:focus {
      border: 1px solid ${scheme.colors.red};
    }
  `}
`
const Warning = styled.span`
  position: absolute;
  top: 100%;
  left: 0;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.red};
`
