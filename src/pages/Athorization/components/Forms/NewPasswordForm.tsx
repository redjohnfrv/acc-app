import React from 'react'
import styled from 'styled-components'
import {useLocation, useNavigate} from 'react-router-dom'
import {Field, Form } from 'react-final-form'
import {PrimaryButton} from '../../../../ui/Buttons'
import {InputPwd} from './FormInputs'
import {FormContainer} from './FormContainer'
import {scheme} from '../../../../assets/styles'
import queryString from 'query-string'
import {validators, composeValidators} from '../../../../helpers'

export const NewPasswordForm = () => {
  const navigate = useNavigate()
  const location = useLocation()
  // const isLoading = useAppSelector(userSelectors.isLoading)
  const resetToken = queryString.parse(location.search)?.passwordResetToken

  function onSubmit(values: { password: string }) {
    const payload = {
      password: values.password,
      resetToken,
    }
    // dispatch(userActions.changePassword(payload))
    //   .then((action: AnyAction) => {
    //     if (action.type.endsWith('/fulfilled')) {
    //       navigate(TO_LOGIN)
    //     }
    //   })
  }
  return (
    <FormContainer>
      <Form
        onSubmit={onSubmit}
        render={({ handleSubmit, submitting, pristine, invalid }) => (
          <form onSubmit={handleSubmit}>
            <Title>New Password</Title>
            <TextContent>Сome up with a new password.</TextContent>
            <Field
              name="password"
              label="Password"
              placeholder="Enter new password"
              component={InputPwd}
              validate={composeValidators(validators.required, validators.passwordLength)}
            />
            <PrimaryButton
              text="Done"
              disabled={submitting || pristine || invalid}
              // loading={isLoading}
              loading={false}
            />
          </form>
        )}
      />
    </FormContainer>
  )
}

const Title = styled.h2`
    margin-bottom: 25px;
`
const TextContent = styled.span`
  display: block;
  margin-bottom: 32px;
  font-size: ${scheme.size.textNormal};
  color: ${scheme.colors.black};
`
