import React, {ReactNode} from 'react'
import styled from 'styled-components'
import {mediaMedium, scheme} from '../../../../assets/styles'

type Props = {
  children: ReactNode
}

export const FormContainer = ({children}: Props) => {
  return <FormWrapper>{children}</FormWrapper>
}

const FormWrapper = styled.div`
  max-width: 454px;
  height: max-content;
  margin: 80px auto 0 auto;
  padding: 40px;
  background: ${scheme.colors.white};
  
  @media (max-width: ${mediaMedium}) {
    border-radius: 6px;
    padding: 24px 16px;
  }
  
  & form {
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`
