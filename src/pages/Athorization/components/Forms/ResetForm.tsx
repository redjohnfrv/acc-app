import React from 'react'
import styled from 'styled-components'
import {Field, Form } from 'react-final-form'
import {FormContainer} from './FormContainer'
import {Input} from './FormInputs'
import {PrimaryButton} from '../../../../ui/Buttons'
import {useNavigate} from 'react-router-dom'
import {SUBMIT, TO_RESEND_LINK} from '../../../../constants'
import {scheme} from '../../../../assets/styles'
import {validators, composeValidators} from '../../../../helpers'


type FormValues = {
  email: string
}

export const ResetForm = () => {
  const navigate = useNavigate()
  // const isLoading = useAppSelector(userSelectors.isLoading)

  const onSubmit = (values: FormValues) => {
    navigate(TO_RESEND_LINK, {
      state: {email: values.email},
    })
  }

  return (
    <FormContainer>
      <Form
        onSubmit={onSubmit}
        render={({handleSubmit, submitting, pristine, invalid}) => (
          <form onSubmit={handleSubmit}>
            <Title>Password Reset</Title>
            <TextContent>Enter your email to receive instructions on how to reset your password.</TextContent>
            <Field
              name="email"
              label="Email"
              placeholder="Enter email ..."
              component={Input}
              validate={composeValidators(validators.required, validators.validEmail)}
            />
            <PrimaryButton
              text="Reset"
              type={SUBMIT}
              disabled={submitting || pristine || invalid}
              // loading={isLoading}
              loading={false}
            />
          </form>
        )}
      />
    </FormContainer>
  )
}

const Title = styled.h2`
    margin-bottom: 25px;
`
const TextContent = styled.span`
  font-size: ${scheme.size.textNormal};
  color: ${scheme.colors.black};
  margin-bottom: 32px;
`
