import React from 'react'
import {FieldRenderProps} from 'react-final-form'
import styled from 'styled-components'
import {images} from '../../../../../assets/images'
import {mediaMedium, scheme} from '../../../../../assets/styles'

type CheckBoxType = {
  label?: string
  disabled?: boolean
}

type Props = FieldRenderProps<string> & CheckBoxType

export const Checkbox = ({input, label, disabled}: Props) => {
  return (
    <CheckboxWrapper>
      <label>
        <span>{label}</span>
        <input {...input} disabled={disabled} />
        <CheckboxImage checked={input.checked} />
      </label>
    </CheckboxWrapper>
  )
}

const CheckboxWrapper = styled.div`
  width: max-content;
  
  & label {
    display: flex;
    flex-direction: row-reverse;
    justify-content: flex-end;
    align-items: center;
  }
  
  & input {
    position: absolute;
    width: 0;
    height: 0;
    z-index: -10;
    opacity: 0;
  }
  
  & span {
    font-size: ${scheme.size.textSmall};
    color: ${scheme.colors.black};

    @media (max-width: ${mediaMedium}) {
      font-size: ${scheme.size.textNormal};
    }
  }
`

const CheckboxImage = styled.div<{checked?: boolean}>`
  width: 20px;
  height: 20px;
  background: ${props => props.checked 
          ? `url(${images.icons.checkboxIn}) center / cover no-repeat` 
          : `url(${images.icons.checkboxOut}) center / cover no-repeat` };
  margin-right: 11px;
`
