import React from 'react'
import styled from 'styled-components'
import {FormContainer} from './FormContainer'
import {TimeoutButton} from '../../../../ui/Buttons'
import {useLocation} from 'react-router-dom'
import {scheme} from '../../../../assets/styles'

export const ResendLinkForm = () => {
  const location = useLocation()
  const email = location.state?.email

  const onClick = () => {
    return email
  }

  return (
    <FormContainer>
      <Title>Password Reset</Title>
      <TextContent>A link was sent to your email to confirm password reset and create a new one.</TextContent>
      <TimeoutButton
        text="Resend"
        type="button"
        disabled={!email}
        onClick={onClick}
        timeout={20}
        timeoutOnMount={false}
      />
    </FormContainer>
  )
}

const Title = styled.h2`
    margin-bottom: 25px;
`
const TextContent = styled.span`
  display: block;
  font-size: ${scheme.size.textNormal};
  color: ${scheme.colors.black};
  margin-bottom: 40px;
`
