import React from 'react'
import styled from 'styled-components'
import {Field, Form } from 'react-final-form'
import {FormContainer} from './FormContainer'
import {Input} from './FormInputs'
import {InputPwd} from './FormInputs'
import {Checkbox} from './FormCheckbox'
import {PrimaryButton} from '../../../../ui/Buttons'
import {Link} from 'react-router-dom'
import {BLANK, SUBMIT, TO_REGISTER, TO_RESET } from '../../../../constants'
import {mediaMedium, scheme} from '../../../../assets/styles'
import {images} from '../../../../assets/images'
import {validators , composeValidators} from '../../../../helpers'
import {useAppDispatch, useAppSelector} from '../../../../redux/hooks'
import {userActions, userSelectors } from '../../../../redux/user'


type FormValues = {
  email: string
  password: string
  remember: boolean
}

export const LoginForm = () => {

  const dispatch = useAppDispatch()
  const isLoading = useAppSelector(userSelectors.isLoading)

  function onSubmit(values: FormValues) {
    const payload = {
      email: values.email,
      password: values.password,
    }
    dispatch(userActions.loginUser(payload))
  }

  return (
    <FormContainer>
      <Form
        onSubmit={onSubmit}
        render={({handleSubmit, submitting, pristine, invalid}) => (
          <form onSubmit={handleSubmit}>
            <Title>Welcome to Accelerist</Title>
            <Tabs>
              <Tab to={TO_REGISTER}>
                Register
              </Tab>
              <Tab data-active to={BLANK}>
                Login
              </Tab>
            </Tabs>
            <Field
              name="email"
              label="Email"
              placeholder="Enter email ..."
              component={Input}
              validate={composeValidators(validators.required, validators.validEmail)}
            />
            <Field
              name="password"
              label="Password"
              placeholder="Enter password"
              customStyle={'margin-bottom: 24px'}
              component={InputPwd}
              validate={composeValidators(validators.required, validators.passwordLength)}
            />
            <Additional>
              <Field
                name="remember"
                label="Remember"
                type="checkbox"
                component={Checkbox}
              />
              <Link to={TO_RESET}>Forgot password?</Link>
            </Additional>
            <PrimaryButton
              text="Login"
              type={SUBMIT}
              disabled={submitting || pristine || invalid}
              loading={isLoading}
            />
            <Continue>
              <span>or continue with</span>
              <a href={BLANK}><img src={images.icons.social} alt="social" /></a>
            </Continue>
          </form>
        )}
      />
    </FormContainer>
  )
}

const Title = styled.h2`
    margin-bottom: 25px;
`
const Tabs = styled.div`
  display: flex;
  width: 100%;
  margin-bottom: 32px;
  padding: 2px;
  background: ${scheme.colors.lightGray};
  border-radius: 6px;
`
const Tab = styled(Link)<{'data-active'?: boolean}>`
  flex: 1;
  height: 40px;
  line-height: 40px;
  background: ${props => props['data-active'] ? scheme.colors.lightBlue : scheme.colors.lightGray};
  text-align: center;
  font-size: ${scheme.size.textSmall};
  color: ${scheme.colors.darkGray};
  border-radius: 6px;
  cursor: pointer;
`
const Additional = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  margin-top: -10px;
  margin-bottom: 60px;
  
  @media (max-width: ${mediaMedium}) {
    margin-bottom: 20px;
  }
`
const Continue = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  
  & span {
    font-size: ${scheme.size.textSmall};
    color: ${scheme.colors.darkGray};
    margin: 32px 0 24px 0;
  }
  
  & img {
    width: 44px;
    height: 44px;
  }
`
