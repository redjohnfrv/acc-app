import React, {ReactNode} from 'react'
import styled from 'styled-components'
import {images} from '../../../assets/images'
import {mediaMedium, scheme} from '../../../assets/styles'

type Props = {
  children: ReactNode
}

export const Container = ({children}: Props) => {
  return (
    <ContainerWrapper>
      <HeaderLine>
        <Logo>
          <img src={images.pics.logo} alt="accelerist" />
          <span>ACCELERIST</span>
        </Logo>
      </HeaderLine>
      {children}
    </ContainerWrapper>
  )
}

const ContainerWrapper = styled.div`
  max-width: 1440px;
  min-height: 100vh;
  height: 100%;
  padding: 80px 0;
  background: url(${images.pics.bg}) center / cover no-repeat;
  
  @media (max-width: ${mediaMedium}) {
    padding: 10px;
  }
`
const HeaderLine = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 80px;
  background: ${scheme.colors.black};
`
const Logo = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  
  & img {
    width: 36px;
    height: 36px;
    margin-right: 16px;
  }
  
  & span {
    font-size: 18px;
    color: ${scheme.colors.white};
    letter-spacing: 1px;
  }
`
