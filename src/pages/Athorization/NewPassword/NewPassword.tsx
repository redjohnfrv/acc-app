import React from 'react'
import {NewPasswordForm} from '../components/Forms'
import {ReturnToLogin} from '../components/ReturnToLogin'
import {Container} from '../ui'

export const NewPassword = () => {
  return (
    <Container>
      <NewPasswordForm />
      <ReturnToLogin />
    </Container>
  )
}
