import React from 'react'
import {Container} from '../ui'
import {LoginForm} from '../components/Forms'

export const Login = () => {
  return (
    <Container>
      <LoginForm />
    </Container>
  )
}
