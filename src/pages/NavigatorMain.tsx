import React from 'react'
import {useAppSelector} from '../redux/hooks'
import {userSelectors} from '../redux/user'
import {NavigatorApp} from './Application'
import {NavigatorAuth} from './Athorization'

export const NavigatorMain = () => {

  const isAuth = useAppSelector(userSelectors.isAuth)

  console.log('Authorization: ', isAuth)
  console.log('token: ', localStorage.getItem('token'))

  return (
    isAuth ? <NavigatorApp /> : <NavigatorAuth />
  )
}
