import {useSwitcher} from './useSwitcher'
import {useEffect, useState} from 'react'
import {TIME} from '../constants'

export const useTimer = (timeout: number, msg: string) => {

  const disableBtn = useSwitcher()
  const initTimer = `0:${timeout}`
  const initSessionStorageTime = Number(sessionStorage.getItem(TIME)) === 0
    ? String(timeout)
    : sessionStorage.getItem(TIME)
  const [timer, setTimer] = useState(initTimer)
  const [time, setTime] = useState(Number(initSessionStorageTime))

  useEffect(() => {
    if (time > 0) {
      setTimeout(() => {
        setTime(time - 1)
        sessionStorage.setItem(TIME, String(time - 1))
      }, 1000)
      time >= 10
        ? setTimer(`00:${time}`)
        : setTimer(`00:0${time}`)
    } else {
      setTimer(msg)
      disableBtn.on()
    }
  },[time, disableBtn, timeout, msg])

  return {timer, disableBtn}

}
